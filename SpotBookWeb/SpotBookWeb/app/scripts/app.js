"use strict";

angular
  .module("SpotsIn", [
    "ngAnimate",
    "ngResource",
    "ngRoute",
    "ngSanitize",
    "ngTouch", ,
    "ui.router",
    "ct.ui.router.extras",
    "ngStorage",
    "ajoslin.promise-tracker"
  ])
  .factory("authInterceptor", function ($rootScope, $q, $window) {
      return {
          request: function (config) {
              config.headers = config.headers || {};
              if ($window.sessionStorage.token) {
                  config.headers.Authorization = "Token " + $window.sessionStorage.token;
              }
              return config;
          },
          responseError: function (rejection) {
              if (rejection.status === 401) {
                  // User is not logged in.
                  $rootScope.$emit("NOT_LOGGED_IN");
              }

              return $q.reject(rejection);
          }
      };
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push("authInterceptor");
  })
  .config(function ($stateProvider, $urlRouterProvider, $stickyStateProvider) {
      //$stickyStateProvider.enableDebug(true);

      $urlRouterProvider.otherwise("/start")
      $urlRouterProvider.when("/", "/start");
      $urlRouterProvider.when("/main", "/main/map");

      $stateProvider
        .state("start", {
            url: "/start",
            views: {
                "rootview": {
                    templateUrl: "views/start.html",
                    controller: "StartCtrl"
                }
            }
        })

        .state("dialogs", {
            url: "/dialogs",
            abstract: true,
            views: {
                "dialogview": {
                    template: "<div ui-view></div>"
                }
            }
        })
        .state("dialogs.register", {
            url: "/register",
            templateUrl: "views/dialogs.register.html",
            controller: "DialogRegisterCtrl"
        })

        .state("main", {
            url: "/main",
            abstract: true,
            views: {
                "rootview": {
                    templateUrl: "views/main.html",
                    controller: "MainCtrl"
                }
            }
        })
        .state("main.map", {
            url: "/map",
            // I couldn't get the sticky shit to work. The map template is included manually now
            // and toggles visibility on this state so that it keeps its inner state.
            sticky: true,
            //views: {
            //    "mainview@main": {
            //        templateUrl: "views/main.map.html",
            //        controller: "MainMapCtrl"
            //    }
            //}
        })
        .state("main.addspot", {
            url: "/addspot",
            views: {
                "dialogview@main": {
                    templateUrl: "views/main.addspot.html",
                    controller: "MainAddSpotCtrl"
                }
            }
        })
        .state("main.addspotbook", {
            url: "/addspotbook",
            views: {
                "dialogview@main": {
                    templateUrl: "views/main.addspotbook.html",
                    controller: "MainAddSpotBookCtrl"
                }
            }
        })
        .state("main.editspotbook", {
            url: "/editspotbook",
            views: {
                "dialogview@main": {
                    templateUrl: "views/main.editspotbook.html",
                    controller: "MainEditSpotBookCtrl"
                }
            }
        });
  })
  .run(function ($rootScope, $state) {
      $(function () {
          FastClick.attach(document.body);
      });

      $rootScope.$state = $state;

      $rootScope.$on("NOT_LOGGED_IN", function () {
          $state.go("start");
      });
  });
