﻿angular.module("SpotsIn").filter("sbAccessRightDisplay", function () {
    return function (input) {
        if (input == 1) {
            return "Ansehen"
        }
        else if (input == 2) {
            return "Spots erstellen"
        }
        else if (input == 3) {
            return "Ansehen und Spots erstellen"
        }
        else {
            return "kein Zugriff"
        }
    };
});