"use strict";

angular.module("SpotsIn")
  .controller("MainAddSpotCtrl",function ($scope, $q, $state, $rootScope,
                                          Spot, geocoder, sbPromiseTracker, sbParamsTransfer,
                                          sbIconHelper, SpotBooks) {
      $scope.availableTypes = sbIconHelper.availableIcons;

      var input;
      function reset() {
          input = {
              city: "Berlin",
              location: null,
              type: $scope.availableTypes["star"]
          };
          $scope.input = input;
      }
      reset();

      var locpreset = sbParamsTransfer.get("newspotlocation");
      if (locpreset) {
          input.location = locpreset.lat + "," + locpreset.lon;
      }
      sbParamsTransfer.clear();

      $scope.cancel = function () {
          $state.go("main.map");
      };

      $scope.create = function () {
          var newSpot = new Spot();
          newSpot.summary = input.summary;
          newSpot.notes = input.notes || "";
          newSpot.address = (input.street || "") + " " + (input.postalCode || "") + " " + (input.city || "");
          newSpot.location = parseLocation();
          if (input.type) {
              newSpot.type = input.type.icon;
          }
          newSpot.type = newSpot.type || "";
          newSpot.spotBookId = SpotBooks.selected.id;

          var deferred = $q.defer();
          var p = newSpot.$save({}, function (response) {
              var createdId = response.spotId || response.id;
              sbParamsTransfer.set("createdspotid", createdId);
              reset();
              $rootScope.$emit("spotadded", newSpot);
              $state.go("main.map");
              deferred.resolve();
          }, function (error) {
              alert("Fehler: " + error.statusText);
              deferred.reject();
          });
          sbPromiseTracker.addPromise(deferred.promise);
      };

      function parseLocation() {
          try {
              if (input.location) {
                  var split = input.location.split(",");
                  var lat = parseFloat(split[0]);
                  var lon = parseFloat(split[1]);
                  return new GeoLocation(lat, lon);
              }
              else {
                  return null;
              }
          }
          catch(e) {
              return null;
          }
      }

      $scope.isInputValid = function () {
          return parseLocation() && input.summary;
      };

      $scope.geolocate = function () {
          var p = geocoder(input.street, input.postalCode, input.city).then(function (response) {
              var loc = response.data;
              input.location = loc.lat + "," + loc.lon;
          }, function (error) {
              input.location = error.statusText;
          });
          sbPromiseTracker.addPromise(p);
      };
  });
