﻿angular.module("SpotsIn").controller("MainMapCtrl", function ($rootScope, $scope, promiseTracker) {

    var tracker = promiseTracker();

    $scope.center = {
        lat: 52.459194,
        lon: 13.333892
    };

    $scope.$watch("center", function (newVal) {
    });

    $scope.isLoading = function () {
        return tracker.active();
    };
});