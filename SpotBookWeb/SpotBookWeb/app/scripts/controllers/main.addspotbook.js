﻿angular.module("SpotsIn")
    .controller("MainAddSpotBookCtrl", function ($scope, $state, $http, sbPromiseTracker, SpotBooks) {
        var input = {};
        $scope.input = input;

        $scope.create = function () {
            if (!$scope.isInputValid()) {
                return;
            }
            var p = SpotBooks.create(input.name);
            sbPromiseTracker.addPromise(p);
            $state.go("main.map");
        };

        $scope.cancel = function () {
            $state.go("main.map");
        };

        $scope.isInputValid = function () {
            return !!input.name;
        };
    });