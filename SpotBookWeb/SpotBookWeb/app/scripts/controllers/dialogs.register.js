﻿"use strict";

angular.module("SpotsIn")
  .controller("DialogRegisterCtrl", function ($scope, $q, $state, $rootScope, $http,
											  sbPromiseTracker, sbApiPath, sbCrypto) {

  	var input = {};
  	$scope.input = input;

  	$scope.cancel = function () {
  		$state.go("start");
  	};

  	$scope.register = function () {
  		var p = $http.post(sbApiPath.get("/auth/register"), {
  			username: input.username,
  			password: sbCrypto.hash(input.password)
  		}).then(function (response) {
  			$state.go("start");
  		});
  		sbPromiseTracker.addPromise(p);
  	};

  	$scope.cancel = function () {
  		$state.go("start");
  	}

  	$scope.isInputValid = function () {
  		return input.username && input.password;
  	};
  });
