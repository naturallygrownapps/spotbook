"use strict";

angular.module("SpotsIn")
  .controller("MainCtrl", function ($rootScope, $scope, $state, promiseTracker,
                                    SpotBooks, sbParamsTransfer, sbSession) {

      SpotBooks.init();

      $rootScope.$on("updatespotbooks", function () {
          updateSpotBooks();
      });

      function updateSpotBooks() {
          return SpotBooks.get().then(function (result) {
              $scope.spotBooks = result;
          });
      }

      $scope.isSidebarVisible = false;
      $scope.toggleSidebar = function () {
          var newValue = !$scope.isSidebarVisible

          if (newValue) {
              updateSpotBooks();
          }
              
          $scope.isSidebarVisible = newValue;
      };

      $scope.getSelectedSpotBook = function () {
          return SpotBooks.selected;
      };

      $scope.selectSpotBook = function (sb) {
          SpotBooks.select(sb);
      };

      $scope.canEditSpotBook = function (sb) {
          return sb.creator.username === sbSession.getUsername();
      };

      $scope.editSpotBook = function (sb) {
          sbParamsTransfer.set("editedspotbook", sb);
          $state.go("main.editspotbook");
      };

  });
