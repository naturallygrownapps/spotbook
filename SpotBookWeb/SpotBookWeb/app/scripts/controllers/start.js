﻿"use strict";

angular.module("SpotsIn")
  .controller("StartCtrl", function ($scope, $q, $http, $window, $state, $timeout,
                                     sbPromiseTracker, sbCrypto, sbSession, SpotBooks,
                                     sbParamsTransfer, sbSpotsManager, sbDeferredPromise) {

      var input = {};
      $scope.input = input;

      function reset() {
          sbSession.clear();
          SpotBooks.clear();
          sbParamsTransfer.clear();
          sbSpotsManager.clear();
      }

      $scope.login = function () {
          reset();
          var p = sbDeferredPromise(function () {
              return $http.post(sbConfig.server + "/auth/login", {
                  username: input.username,
                  password: sbCrypto.hash(input.password)
              }).then(function (response) {
                  sbSession.loggedIn(input.username);
                  $window.sessionStorage.token = response.data.token;
                  return SpotBooks.init().then(function () {
                      $state.go("main.map");
                  });
              });
          });

          sbPromiseTracker.addPromise(p);
      };

      $scope.register = function () {
          $state.go("dialogs.register");
      };

      $scope.logout = function () {
          reset();
      };

      $scope.isInputValid = function () {
          return input.username && input.password;
      };
  });
