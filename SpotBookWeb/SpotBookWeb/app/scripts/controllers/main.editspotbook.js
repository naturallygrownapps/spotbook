﻿angular.module("SpotsIn")
    .controller("MainEditSpotBookCtrl", function ($rootScope, $scope, $state,
                                                  sbParamsTransfer, sbPromiseTracker, sbSession,
                                                  SpotBooks) {
        var edited = sbParamsTransfer.get("editedspotbook");
        if (!edited) {
            //#if DEBUG
            //edited = {
            //    name: "Test",
            //    rights: [
            //        { user: { username: "rufus"}, accessRight: 1 }
            //    ]
            //};
            //#endif

            console.error("Error: Should have passed a SpotBook for editing.");
            $state.go("main.map");
        }
        sbParamsTransfer.clear();

        var initialUpdate = SpotBooks.update(edited);
        sbPromiseTracker.addPromise(initialUpdate);

        var input = {
            // For later editing of the name.
            name: edited.name
        };
        $scope.input = input;
        $scope.edited = edited;
        function clearAddRightInput() {
            input.addright = {
                username: "",
                right: 1
            };
        }
        clearAddRightInput();

        $scope.close = function () {
            $state.go("main.map");
        };

        $scope.addRight = function () {
            var p = SpotBooks.addRight($scope.edited,
                                       input.addright.username,
                                       input.addright.right)
                    .then(function () {
                        clearAddRightInput();
                        return SpotBooks.update($scope.edited);
                    });
            sbPromiseTracker.addPromise(p);
        };

        $scope.canRemoveRight = function (right) {
            return right.user.username !== sbSession.getUsername();
        };

        $scope.removeRight = function (right) {
            var p = SpotBooks.removeRight($scope.edited, right)
                    .then(function () {
                        return SpotBooks.update($scope.edited);
                    });
            sbPromiseTracker.addPromise(p);
        };

        $scope.isAddRightInputValid = function () {
            return input.addright.username && input.addright.right > 0 && input.addright.right < 4;
        };

        $scope.deleteSpotBook = function () {
            if (confirm('Wirklich das SpotBook "' + edited.name + '" vollständig löschen? Alle zugehörigen Spots werden ebenfalls gelöscht!')) {
                var p = SpotBooks.delete(edited).then(function () {
                    $rootScope.$emit("updatespotbooks");
                    $scope.close();
                });

                sbPromiseTracker.addPromise(p);
            }
        };

    });