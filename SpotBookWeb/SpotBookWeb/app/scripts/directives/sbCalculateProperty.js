﻿angular.module("SpotsIn").directive("sbCalculateProperty", function () {
    return {
        restrict: "A",
        scope: {
            sbCalculateProperty: "@",
            targetProperty: "@"
        },
        link: function (scope, elem) {

            function evaluate() {
                if (scope.sbCalculateProperty && scope.targetProperty) {
                    var h = eval(scope.sbCalculateProperty);
                    elem.css(scope.targetProperty, h);
                }
            }

            $(window).resize(function () {
                evaluate();
            });

            evaluate();
        }
    }
});