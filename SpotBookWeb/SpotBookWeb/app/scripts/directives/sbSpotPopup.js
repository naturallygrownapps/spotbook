﻿angular.module("SpotsIn").directive("sbSpotPopup", function ($q, $rootScope, $timeout,
                                                             Spot, sbPromiseTracker) {
    return {
        restrict: "A",
        templateUrl: "views/directives/sbSpotPopup.html",
        //replace: true,
        scope: {
            spot: "=",
            lPopup: "="
        },
        link: function (scope, elem) {
            scope.optionsVisible = false;
            var optionsDiv = elem.find(".col2").first();
            optionsDiv.hide();

            function popupClose() {
                $timeout(function () {
                    hideOptions();
                });
            }

            scope.$watch("lPopup", function (newVal, oldVal) {
                if (oldVal) {
                    oldVal.off("popupclose", popupClose);
                }
                if (newVal) {
                    newVal.on("popupclose", popupClose);
                }
            });

            function showOptions() {
                optionsDiv.show();

                if (scope.lPopup && typeof scope.lPopup.update == "function") {
                    // re-render the popup.
                    scope.lPopup.update();
                }
                scope.optionsVisible = true;
            }

            function hideOptions() {
                optionsDiv.hide();

                if (scope.lPopup && typeof scope.lPopup.update == "function") {
                    // re-render the popup.
                    scope.lPopup.update();
                }
                scope.optionsVisible = false;
            }

            scope.toggleOptions = function () {
                scope.optionsVisible = !scope.optionsVisible;

                if (scope.optionsVisible) {
                    showOptions();
                }
                else {
                    hideOptions();
                }
            };

            scope.remove = function () {
                if (confirm("Diesen Spot wirklich löschen?")) {
                    var params = { id: scope.spot.id };
                    var deferred = $q.defer();
                    Spot.removespot(params, function () {
                        $rootScope.$emit("spotdeleted", scope.spot);
                        $rootScope.$emit("updatemap");
                        deferred.resolve();
                    }, function () {
                        deferred.reject();
                    });
                    sbPromiseTracker.addPromise(deferred.promise);
                }
            };
        }
    };
});