﻿angular.module("SpotsIn").directive("sbRequireStates", function ($state) {
    return {
        restrict: "A",
        scope: {
            sbRequireStates: "="
        },
        transclude: true,
        replace: true,
        template: '<div ng-show="showElement();" ng-transclude></div>',
        link: function (scope, elem) {

            scope.showElement = function () {
                if (!scope.sbRequireStates) {
                    return false;
                }
                else if (angular.isArray(scope.sbRequireStates)) {
                    var show = false;
                    angular.forEach(scope.sbRequireStates, function (i) {
                        show |= $state.includes(i);
                    });
                    return show;
                }
                else {
                    return $state.includes(scope.sbRequireStates);
                }
            };
        }
    };
});