﻿angular.module("SpotsIn").directive("sbMapControl", function ($document, $window, $q, $timeout, $compile,
                                                              $rootScope, $state,
                                                              sbParamsTransfer, promiseTracker, sbPromiseTracker, Spot,
                                                              sbUtils, sbIconHelper, sbSpotsManager, SpotBooks) {
    return {
        restrict: "A",
        templateUrl: "views/directives/sbMapControl.html",
        replace: true,
        scope: {
            center: "="
        },
        link: function (scope, elem) {

            var mapElem = elem.find("#map-element:first");

            // Layout.
            var header;
            function updateHeight() {
                var headerHeight = 0;
                if (header) {
                    headerHeight = header.outerHeight();
                }
                var newHeight = $($window).outerHeight() - headerHeight;
                mapElem.height(newHeight);
            }

            $document.ready(function () {
                header = $("#header");

                $($window).resize(function () {
                    updateHeight();
                });
                updateHeight();
            });

            // Additional Setup.
            var tracker = promiseTracker();

            // Setup map.
            var map = L.map(mapElem[0], { zoomControl: true });

            //var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            //var osmUrl = "http://84.201.32.208:8080/solar-light/{z}/{x}/{y}.png";
            //var osmUrl = "http://tiles.spotsin.berlin/{z}/{x}/{y}.png";
            var osmUrl = "http://tiles.spotsin.berlin/1.0.0/osm_spotsin/{z}/{x}/{y}.png";
            var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
            var osm = new L.TileLayer(osmUrl, { minZoom: 8, maxZoom: 18, attribution: osmAttrib })
                           .addTo(map);

            // Locator control.
            var LocationControl = L.Control.extend({
                options: {
                    position: "topleft",
                },
                onAdd: function (map) {
                    var tplFn = $compile("<div class='leaflet-control-location leaflet-bar leaflet-control' " +
                                         "ng-click='gotoLocation();$event.stopPropagation();'>" +
                                         "<a class='leaflet-bar-part leaflet-bar-part-single'" +
                                         "ng-class=\"{'leaflet-rotate-spinner': isLocating, 'icomoon-spinner': isLocating, " +
                                         "'icomoon-target': !isLocating, 'location-enabled': isLocationEnabled}\"></a></div>");
                    var container = tplFn(scope);
                    return container[0];
                }
            });
            map.addControl(new LocationControl());

            // Map spots.
            var SpotIcon = L.Icon.extend({
                options: {
                    //shadowUrl: 'leaf-shadow.png',
                    //iconUrl: "../resources/images/icon_placeholder.png",
                    iconSize: [40, 54],
                    shadowSize: [0, 0],
                    iconAnchor: [20, 52],
                    shadowAnchor: [0, 0],
                    popupAnchor: [0, -44]
                },
                createIcon: function () {
                    var i = $("<i></i>");
                    this._setIconStyles(i[0], "icon");
                    i.addClass("spot-marker-icon");
                    return i[0];
                },
            });
            function createSpotIcon(iconName) {
                return new SpotIcon({ className: "icon-" + iconName });
            }

            var ClusterIcon = L.DivIcon.extend({
                options: {
                    iconSize: [40, 54],
                    iconAnchor: [20, 52],
                    popupAnchor: [0, -44],
                    childCount: 0
                },
                createIcon: function () {
                    var span = $("<span>" + this.options.childCount + "</span>");
                    this._setIconStyles(span[0], "icon");
                    span.addClass("spot-marker-icon cluster");
                    return span[0];
                },
            });

            // All markers are added to a common cluster.
            var markersCluster = new L.MarkerClusterGroup({
                showCoverageOnHover: false,
                zoomToBoundsOnClick: true,
                spiderfyOnMaxZoom: false,
                maxClusterRadius: 50,
                //removeOutsideVisibleBounds: true,
                iconCreateFunction: function (cluster) {
                    return new ClusterIcon({
                        childCount: cluster.getChildCount()
                    });
                }
            });
            map.addLayer(markersCluster);

            function createSpotMarker(spot) {
                var iconName = sbIconHelper.isIconAvailable(spot.type) ? spot.type : "star";
                var bounceOnAdd = false;
                // Only bounce on spots that were just added. If we don't perform this check,
                // all spots, even those that existed on the server already, will bounce.
                var createdSpotId = sbParamsTransfer.get("createdspotid");
                if (createdSpotId && spot.id === createdSpotId) {
                    sbParamsTransfer.clear("createdspotid");
                    bounceOnAdd = true;
                }
                var marker = L.marker(new L.LatLng(spot.location.latitude,
                                                   spot.location.longitude),
                                      {
                                          icon: createSpotIcon(iconName),
                                          bounceOnAdd: bounceOnAdd
                                      });
                return marker;
            }

            var loadedSpotDict = {};
            function getSpotsDiff(receivedSpots, center, radius) {

                // A dictionary of the received spots (id -> spot).
                var receivedSpotsDict = {};
                for (var i = 0; i < receivedSpots.length; i++) {
                    var s = receivedSpots[i];
                    receivedSpotsDict[s.id] = s;
                }

                // Stores all spots that we can remove because they don't exist in the
                // received set anymore.
                var removed = [];

                var centerLat = center.lat;
                var centerLon = center.lng;

                for (var sId in loadedSpotDict) {
                    var s = loadedSpotDict[sId];
                    var lat = s.location.latitude;
                    var lon = s.location.longitude;

                    var dist = sbUtils.getDistanceFromLatLonInKm(centerLat, centerLon, lat, lon);
                    if (dist <= radius) {
                        if (!(sId in receivedSpotsDict)) {
                            // Spot was removed remotely.
                            removed.push(s);
                        }
                        else {
                            // We already have that spot, so we can delete it
                            // so that it is not processed again.
                            delete receivedSpotsDict[sId];
                        }
                    }
                }

                var added = $.map(receivedSpotsDict, function (el) { return el; });

                for (var i = 0; i < added.length; i++) {
                    var s = added[i];
                    loadedSpotDict[s.id] = s;
                }
                for (var i = 0; i < removed.length; i++) {
                    var s = removed[i];
                    delete loadedSpotDict[s.id];
                }

                return {
                    removed: removed,
                    added: added
                };
            }

            function loadSpotsTracked() {
                var p = loadSpots();
                tracker.addPromise(p);
                return p;
            }

            function createPopup(spot) {
                var result = L.popup();

                var popupScope = scope.$new(true);
                popupScope.spot = spot;
                popupScope.lPopup = result;
                var tplFn = $compile("<div sb-spot-popup spot='spot' l-popup='lPopup'></div>");
                var popupHtml = tplFn(popupScope);

                var popup = result.setContent(popupHtml[0]);

                return result;
            };

            function loadSpots() {
                var selectedSb = SpotBooks.selected;
                if (!selectedSb) {
                    return $q.when();
                }

                var center;
                try
                {
                    // getCenter() throws if the map was not initialized before.
                    center = map.getCenter();
                }
                catch (e) { return $q.when(); }
                var bounds = map.getBounds();
                // Increase the range a bit so that we load a bit more than we see.
                bounds = bounds.pad(0.5);
                var mapBoundNorthEast = map.getBounds().getNorthEast();
                var radius = mapBoundNorthEast.distanceTo(center);

                return sbSpotsManager.getSpotsInBB(selectedSb.id,
                                                   bounds.getWest(),
                                                   bounds.getNorth(),
                                                   bounds.getEast(),
                                                   bounds.getSouth()).then(function (spots) {
                                                       var diff = getSpotsDiff(spots, center, radius);

                                                       for (var i = 0; i < diff.added.length; i++) {
                                                           var spot = diff.added[i];
                                                           var marker = createSpotMarker(spot);
                                                           markersCluster.addLayer(marker);
                                                           var popup = createPopup(spot);
                                                           marker.bindPopup(popup);
                                                           spot.marker = marker;
                                                       }

                                                       for (var i = 0; i < diff.removed.length; i++) {
                                                           var spot = diff.removed[i];
                                                           if (spot.marker) {
                                                               markersCluster.removeLayer(spot.marker);
                                                           }
                                                       }
                                                   });
            }

            // Events.
            map.on("moveend", function () {
                $timeout(function () {
                    var center = map.getCenter();
                    scope.center = {
                        lat: center.lat,
                        lon: center.lng
                    };
                });
                loadSpotsTracked();
            });

            var mapOptionsPopup = null;
            function tryCloseMapOptionsPopup() {
                if (mapOptionsPopup) {
                    map.closePopup(mapOptionsPopup);
                    mapOptionsPopup = null;
                    return true;
                }
                else {
                    return false;
                }
            }

            map.on("contextmenu", function (e) {
                //if (!tryCloseMapOptionsPopup()) {
                    var optionsScope = scope.$new(true);
                    optionsScope.location = {
                        lat: e.latlng.lat,
                        lon: e.latlng.lng
                    };
                    var tplFn = $compile("<div sb-map-options-popup location='location'></div>");
                    var popupHtml = tplFn(optionsScope);
                    mapOptionsPopup = L.popup()
                                       .setLatLng(e.latlng)
                                       .setContent(popupHtml[0])
                                       .openOn(map);
                //}
            });

            map.on("popupclose", function (e) {
                var popup = e.popup;
                if (popup && typeof popup.fire === "function") {
                    popup.fire("popupclose");
                }
            });

            scope.$watch("center", function (newVal) {
                if (!newVal)
                    return;
                var lat = newVal.lat;
                var lon = newVal.lon;

                var update = true;
                try {
                    var currentCenter = map.getCenter();
                    if (lat === currentCenter.lat ||
                        lon === currentCenter.lng) {
                        update = false;
                    }
                } catch (e) {
                }

                if (update) {
                    var m = map.setView(L.latLng(lat, lon), 12);
                }
            });

            $rootScope.$on("updatemap", function () {
                tryCloseMapOptionsPopup();
                loadSpotsTracked();
            });

            $rootScope.$on("spotadded", function (e, spot) {
                tryCloseMapOptionsPopup();
                var p = loadSpotsTracked().then(function () {
                    // Scroll to new spot.
                    var pos = L.latLng(spot.location.latitude,
                                       spot.location.longitude);
                    map.setView(pos);
                });
                sbPromiseTracker.addPromise(p);
            });

            $rootScope.$on("selectedspotbookchanged", function (sb) {
                tryCloseMapOptionsPopup();
                loadSpotsTracked();
            });

            scope.isLoading = function () {
                return tracker.active();
            };

            //// Locating the user.
            var locationMarker = null;
            scope.isLocationEnabled = false;
            scope.isLocating = false;
            scope.gotoLocation = function () {
                scope.isLocationEnabled = !scope.isLocationEnabled;
                if (scope.isLocationEnabled) {
                    scope.isLocating = true;
                    map.locate({ setView: true, maxZoom: 16 });
                }
                else if (locationMarker) {
                    map.removeLayer(locationMarker);
                }
            };

            map.on("locationfound", function (e) {
                $timeout(function () {
                    scope.isLocating = false;
                });

                locationMarker = L.circleMarker(e.latlng, {
                    color: "#801638",
                    fillColor: '#593574',
                    fillOpacity: 0.5
                }).addTo(map);
            });

            map.on("locationerror", function (e) {
                $timeout(function () {
                    scope.isLocating = false;
                });
            });

        }
    };
});