﻿angular.module("SpotsIn").directive("sbLoader", function ($rootScope, sbPromiseTracker) {
    return {
        restrict: "A",
        templateUrl: "views/directives/sbLoader.html",
        replace: true,
        link: function (scope, elem) {
            scope.show = function () {
                return sbPromiseTracker.isActive() ||
                       scope.hasError();
            };

            scope.hasError = function () {
                return !!sbPromiseTracker.getError();
            };

            scope.closeError = function () {
                sbPromiseTracker.clearError();
            };

            scope.getError = function () {
                var e = sbPromiseTracker.getError();
                if (e) {
                    var msg = e.description || e.statusText || e.status;
                    if (msg) {
                        return msg;
                    }
                }

                return JSON.stringify(e);
             };
        }
    };
});