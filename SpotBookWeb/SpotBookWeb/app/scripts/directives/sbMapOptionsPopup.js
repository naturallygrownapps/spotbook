﻿angular.module("SpotsIn").directive("sbMapOptionsPopup", function ($state, sbParamsTransfer) {
    return {
        restrict: "A",
        templateUrl: "views/directives/sbMapOptionsPopup.html",
        scope: {
            location: "="
        },
        link: function (scope, elem) {
            scope.add = function () {
                sbParamsTransfer.set("newspotlocation", scope.location);
                $state.go("main.addspot");
            };
        }
    };
});