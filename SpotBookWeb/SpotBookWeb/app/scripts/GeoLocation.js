﻿function GeoLocation(lat, lon) {
    this.type = "Point";
    this.latitude = lat;
    this.longitude = lon;
}