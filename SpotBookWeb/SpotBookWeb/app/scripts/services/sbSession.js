﻿angular.module("SpotsIn").factory("sbSession", function ($window) {
    var svc = {};

    var session = {
        username: null
    };

    svc.clear = function () {
        session = {};
        delete $window.sessionStorage.token;
    };

    svc.loggedIn = function (username) {
        session.username = username;
    };
    svc.getUsername = function () {
        return session.username;
    };

    // @ifdef DEBUG
    svc.loggedIn("rufus");
    // @endif

    return svc;
});