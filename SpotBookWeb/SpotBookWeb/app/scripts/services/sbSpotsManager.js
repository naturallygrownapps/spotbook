﻿angular.module("SpotsIn").factory("sbSpotsManager", function ($rootScope, $q, Spot, sbUtils) {

    var defaultZoom = 14;
    // spotbook id -> cache
    var caches = {};
    var currentCancellation = null;

    function long2tile(lon, zoom) {
        if (!zoom) {
            zoom = defaultZoom;
        }
        return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
    }
    function lat2tile(lat, zoom) {
        if (!zoom) {
            zoom = defaultZoom;
        }
        return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
    }
    function tile2long(x, zoom) {
        if (!zoom) {
            zoom = defaultZoom;
        }
        return (x / Math.pow(2, zoom) * 360 - 180);
    }
    function tile2lat(y, zoom) {
        if (!zoom) {
            zoom = defaultZoom;
        }
        var n = Math.PI - 2 * Math.PI * y / Math.pow(2, zoom);
        return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
    }
    function spot2TileCoords(spot) {
        var lat = spot.location.latitude;
        var lon = spot.location.longitude;
        var tileX = long2tile(lon);
        var tileY = lat2tile(lat);
        return new TileCoordinates(tileX, tileY);
    }

    function CancellationToken() {
        var cancelled = false;
        this.cancel = function () {
            cancelled = true;
        }
        this.isCancelled = function () {
            return cancelled;
        }
    }
    function SpotCache() {
        // spot tile id (x,y) -> SpotTile (spots on this tile)
        var spotTiles = {};
        this.spotTiles = spotTiles;
        // Cache entries are cleared after this period.
        var cachePeriod = 0.5 * 60 * 1000;
        var self = this;

        function getTileForSpot(spot) {
            var tileCoords = spot2TileCoords(spot);
            var tile = self.getSpotTile(tileCoords);
            return tile;
        }

        this.hasSpotTile = function (tileCoords) {
            return tileCoords.getId() in spotTiles;
        };

        this.getSpotTile = function (tileCoords) {
            var id = tileCoords.getId();
            var tile = spotTiles[id];
            if (!tile) {
                tile = new SpotTile();
                spotTiles[id] = tile;
            }
            return tile;
        };

        this.removeSpotTile = function (tileCoords) {
            delete spotTiles[tileCoords.getId()];
        };

        this.clearTiles = function (tileCoordList) {
            for (var i = 0; i < tileCoordList.length; i++) {
                var tile = self.getSpotTile(tileCoordList[i]);
                tile.spots = [];
                // Force the tile to be updated on the next update.
                tile.lastUpdate = new Date(0);
            }
        };

        this.storeSpot = function (spot) {
            var tile = getTileForSpot(spot);
            tile.spots.push(spot);
            tile.lastUpdate = new Date();
        };

        this.deleteSpot = function (spot) {
            var tile = getTileForSpot(spot);
            tile.spots = tile.spots.filter(function (s) {
                return s.id != spot.id;
            });
            tile.lastUpdate = new Date();
        };

        this.getSpots = function (tileCoordList) {
            var allspots = [];
            for (var i = 0; i < tileCoordList.length; i++) {
                var tile = self.getSpotTile(tileCoordList[i]);
                allspots = allspots.concat(tile.spots);
            }
            return allspots;
        }

        /**
         * Removes cache entries that have become too old.
         * The provided parameter specifies the tile to check.
         */
        this.checkSpotTile = function (tileCoords) {
            var nowMs = new Date().getTime();
            if (self.hasSpotTile(tileCoords)) {
                var spotTile = self.getSpotTile(tileCoords);
                var diffMs = nowMs - spotTile.lastUpdate.getTime();
                if (diffMs > cachePeriod) {
                    // This entry is too old, forget it.
                    self.removeSpotTile(tileCoords);
                }
            }
        };
    }
    function SpotTile() {
        this.spots = [];
        this.lastUpdate = new Date();
    }
    function TileCoordinates(x, y) {
        this.x = x;
        this.y = y;

        var self = this;

        this.getId = function () {
            return self.x + "," + self.y;
        };
    }

    function getCache(spotBookId) {
        var cache = caches[spotBookId];
        if (!cache) {
            cache = new SpotCache();
            caches[spotBookId] = cache;
        }
        return cache;
    }

    function getBoundingBoxLatLon(tileCoordList) {
        var minX = Number.MAX_VALUE, minY = Number.MAX_VALUE, maxX = 0, maxY = 0;
        for (var i = 0; i < tileCoordList.length; i++) {
            var t = tileCoordList[i];
            if (t.x < minX) {
                minX = t.x;
            }
            else if (t.x > maxX) {
                maxX = t.x;
            }

            if (t.y < minY) {
                minY = t.y;
            }
            else if (t.y > maxY) {
                maxY = t.y;
            }
        }

        return {
            minX: tile2long(minX),
            minY: tile2lat(minY),
            maxX: tile2long(maxX),
            maxY: tile2lat(maxY)
        };
    }

    function initCancellation() {
        if (currentCancellation) {
            currentCancellation.cancel();
        }
        currentCancellation = new CancellationToken();
    }
    function isCancelled() {
        return currentCancellation ? currentCancellation.isCancelled() : false;
    }

    function getSpotsInBB(spotBookId, left, top, right, bottom) {
        // Cancel all previous requests.
        initCancellation();
        var tileLeft = long2tile(left);
        var tileTop = lat2tile(top);
        var tileRight = long2tile(right);
        var tileBottom = lat2tile(bottom);

        var width = tileRight - tileLeft;
        var height = tileBottom - tileTop;

        var cache = getCache(spotBookId);

        // Determine the set of spot tiles that we need to get from the server.
        var newTileCoords = [];
        var existingTileCoords = [];
        for (var x = 0; x <= width; x++) {
            for (var y = 0; y <= height; y++) {
                var tileCoords = new TileCoordinates(tileLeft + x, tileTop + y);
                cache.checkSpotTile(tileCoords);

                if (!cache.hasSpotTile(tileCoords)) {
                    newTileCoords.push(tileCoords);
                }
                else {
                    existingTileCoords.push(tileCoords);
                }
            }
        }

        var deferred = $q.defer();
        // Check if we need to get new spots first.
        if (newTileCoords.length > 0) {
            var bb = getBoundingBoxLatLon(newTileCoords);
            var params = {
                spotbookid: spotBookId,
                bb: bb.minX + "," + bb.minY + "," + bb.maxX + "," + bb.maxY
            };
            var receivedSpots = Spot.query(params,
                function () {
                    // We only want one "thread" to process tiles at a time.
                    if (isCancelled()) {
                        deferred.reject(error);
                    }
                    // Clear the tiles that we requested because they may have deleted spots.
                    cache.clearTiles(newTileCoords);

                    // Store the new spots.
                    for (var i = 0; i < receivedSpots.length; i++) {
                        var s = receivedSpots[i];
                        cache.storeSpot(s);
                    }

                    // Return the new and previously cached spots.
                    var spots = cache.getSpots(existingTileCoords.concat(newTileCoords));
                    currentCancellation = null;
                    deferred.resolve(spots);
                }, function (error) {
                    currentCancellation = null;
                    deferred.reject(error);
            });
        }
        // Or if we can just return all requested spots because we have them cached.
        else {
            // We have all tiles up to date. Gather the results.
            var allspots = cache.getSpots(existingTileCoords);
            deferred.resolve(allspots);
        }

        return deferred.promise;
    }

    function clear() {
        caches = {};
        initCancellation();
        currentCancellation = null;
    }

    function invalidateTileForSpot(spot) {
        var cache = getCache(spot.spotBookId);
        var tileCoords = spot2TileCoords(spot);
        cache.clearTiles([ tileCoords ]);
    }

    //// Events
    $rootScope.$on("spotadded", function (e, spot) {
        if (spot && spot.spotBookId) {
            var cache = getCache(spot.spotBookId);
            cache.storeSpot(spot);
        }
    });

    $rootScope.$on("spotdeleted", function (e, spot) {
        if (spot && spot.spotBookId) {
            var cache = getCache(spot.spotBookId);
            cache.deleteSpot(spot);
        }
    });

    return {
        getSpotsInBB: getSpotsInBB,
        invalidateTileForSpot: invalidateTileForSpot,
        clear: clear
    };

});