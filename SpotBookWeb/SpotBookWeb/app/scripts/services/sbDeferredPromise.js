﻿/**
 * Takes a factory function that must create a promise and executes that factory after a
 * $timeout. The result of the promise that the factory created is returned as another promise.
 * This is useful for cases where we use the sbPromiseTracker and want it to be shown before tasks 
 * are actually started.
 */
angular.module("SpotsIn").factory("sbDeferredPromise", function ($q, $timeout) {
    return function (promiseFac) {
        var deferred = $q.defer();

        $timeout(function () {
            promiseFac().then(
                function (result) {
                    deferred.resolve(result);
                },
                function (error) {
                    deferred.reject(error);
                });
        });

        return deferred.promise;
    };
});