﻿angular.module("SpotsIn").factory("sbParamsTransfer", function () {

    var params = {};

    function getParam(name) {
        return params[name];
    }

    function setParam(name, value) {
        params[name] = value;
    }

    function clearParams(name) {
        if (name && name in params) {
            delete params[name];
        }
        else {
            params = {};
        }
    }

    return {
        get: getParam,
        set: setParam,
        clear: clearParams
    };
});