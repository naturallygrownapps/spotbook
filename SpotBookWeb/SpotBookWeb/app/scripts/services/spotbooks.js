﻿angular.module("SpotsIn").factory("SpotBooks", function ($rootScope, $resource, $http, $q, $localStorage, 
                                                         sbUtils, sbApiPath) {
    
    var svc = {};

    var res = $resource(sbApiPath.get("/spotbooks/:spotbookid"),
      {}, {
      }
     );

    // The SpotBook that is currently selected by the user.
    svc.selected = null;
    // The SpotBooks of the user. We keep this array for caching purposes and update it on each get() request.
    var books = [];

    svc.clear = function () {
        books = [];
    };

    svc.select = function (sb) {
        svc.selected = sb;
        $localStorage.prevSelectedSpotBookId = sb.id;
        $rootScope.$emit("selectedspotbookchanged", sb);
    };
    svc.get = function () {
        var deferred = $q.defer();

        var receivedBooks = res.query({},
            function () {
                var removedBooksById = {};
                for (var i = 0; i < books.length; i++) {
                    var bI = books[i];
                    removedBooksById[bI.id] = bI;
                }

                for (var i = 0; i < receivedBooks.length; i++) {
                    var b = receivedBooks[i];
                    // Check if we already see that book.
                    var existingBook = sbUtils.find(books, function (e) {
                        return e.id == b.id;
                    });
                    // Only add it if not there yet.
                    if (!existingBook) {
                        books.push(b);
                    }

                    if (b.id in removedBooksById) {
                        delete removedBooksById[b.id];
                    }
                }

                for (var id in removedBooksById) {
                    for (var i = 0; i < books.length; i++) {
                        if (books[i].id === id) {
                            books.splice(i, 1);
                            break;
                        }
                    }
                }

                deferred.resolve(books);
            },
            function (response) {
                deferred.reject(response.status);
            });

        return deferred.promise;
    };

    svc.create = function (name) {
        var newSpotBook = new res();
        newSpotBook.name = name;

        var deferred = $q.defer();
        var p = newSpotBook.$save({}, function (response) {
            var createdId = response.spotBookId || response.id;
            $rootScope.$emit("updatespotbooks");
            deferred.resolve();
        }, function (error) {
            alert("Fehler: " + error.statusText);
            deferred.reject();
        });
        return deferred.promise;
    };

    svc.addRight = function (spotBook, username, right) {
        return $http.post(sbApiPath.get("/spotbooks/" + spotBook.id + "/rights/" + username), {
            accessRight: right
        }).then(function (result) {

        }, function (response) {
            var errCode = response.data.errorCode;
            if (errCode === "UserNotFound") {
                return $q.reject("Der Benutzer '" + username + "' existiert nicht.");
            } else {
                return $q.reject(response);
            }
        });
    };

    svc.removeRight = function (spotBook, right) {
        return $http.delete(sbApiPath.get("/spotbooks/" + spotBook.id + "/rights/" + right.user.username))
        .then(function (result) {

        }, function (response) {
            console.error("Could not remove user right: " + response.status);
            return $q.reject(response);
        });
    };

    svc.update = function (spotBook) {
        if (!spotBook.id) {
            return $q.reject();
        }
        var updated = res.get({ spotbookid: spotBook.id },
            function () {
                var existingBook = sbUtils.find(books, function (e) {
                    return e.id == spotBook.id;
                });
                angular.copy(updated, spotBook);
            });
        return updated.$promise;
    };

    svc.delete = function (spotBook) {
        if (!spotBook.id) {
            return $q.reject();
        }
        var deleted = res.delete({ spotbookid: spotBook.id });
        return deleted.$promise;
    };


    svc.init = function () {
        return svc.get().then(function (result) {
            var prevSelectionId = $localStorage.prevSelectedSpotBookId;
            var foundPrevious = false;
            if (prevSelectionId) {
                var existingBook = sbUtils.find(result, function (e) {
                    return e.id == prevSelectionId;
                });
                if (existingBook) {
                    svc.select(existingBook);
                    foundPrevious = true;
                }
            }

            if (!foundPrevious && result.length > 0) {
                svc.select(result[0]);
            }
        });
    };

    return svc;
});