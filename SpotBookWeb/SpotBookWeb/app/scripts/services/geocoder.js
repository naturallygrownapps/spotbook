﻿angular.module("SpotsIn").factory("geocoder", function ($http) {
    return function (streetWithHouseNumber, postalCode, city) {
        var url = sbConfig.server + "/geocode/" + streetWithHouseNumber + "/" + city + "/" + postalCode;
        return $http.get(url);
    };
});