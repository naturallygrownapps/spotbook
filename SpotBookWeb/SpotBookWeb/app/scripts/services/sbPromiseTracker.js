﻿angular.module("SpotsIn").factory("sbPromiseTracker", function ($rootScope, $q, promiseTracker) {
    var tracker = promiseTracker();
    $rootScope.loadingTracker = tracker;

    var error = null;

    function addPromise(promise) {
        var p = promise.then(function() {
        }, function (e) {
            error = e;
            return $q.reject();
        });
        tracker.addPromise(p);
    }

    function getError() {
        return error;
    }

    function clearError() {
        error = null;
    }

    function isActive() {
        return tracker.active();
    }

    return {
        addPromise: addPromise,
        isActive: isActive,
        tracker: tracker,
        getError: getError,
        clearError: clearError
    };

});