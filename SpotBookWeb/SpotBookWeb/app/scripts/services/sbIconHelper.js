﻿angular.module("SpotsIn").factory("sbIconHelper", function () {
    //var availableIcons = [
    //    "airfield",
    //    "airport",
    //    "alcohol-shop",
    //    "art-gallery",
    //    "bakery",
    //    "bank",
    //    "bar",
    //    "beer",
    //    "bicycle",
    //    "building",
    //    "bus",
    //    "cafe",
    //    "camera",
    //    "campsite",
    //    "car",
    //    "cemetery",
    //    "cinema",
    //    "circle-stroked",
    //    "circle",
    //    "city",
    //    "clothing-store",
    //    "college",
    //    "commercial",
    //    "cross",
    //    "dam",
    //    "danger",
    //    "disability",
    //    "dog-park",
    //    "embassy",
    //    "emergency-telephone",
    //    "entrance",
    //    "farm",
    //    "fast-food",
    //    "ferry",
    //    "fire-station",
    //    "fuel",
    //    "garden",
    //    "golf",
    //    "grocery",
    //    "hairdresser",
    //    "harbor",
    //    "heart",
    //    "heliport",
    //    "hospital",
    //    "icons",
    //    "industrial",
    //    "land-use",
    //    "laundry",
    //    "library",
    //    "lighthouse",
    //    "lodging",
    //    "logging",
    //    "london-underground",
    //    "marker-stroked",
    //    "marker",
    //    "minefield",
    //    "mobilephone",
    //    "monument",
    //    "museum",
    //    "music",
    //    "oil-well",
    //    "park",
    //    "park2",
    //    "parking-garage",
    //    "parking",
    //    "pharmacy",
    //    "pitch",
    //    "place-of-worship",
    //    "playground",
    //    "police",
    //    "polling-place",
    //    "post",
    //    "prison",
    //    "rail-above",
    //    "rail-light",
    //    "rail-metro",
    //    "rail-underground",
    //    "rail",
    //    "religious-christian",
    //    "religious-jewish",
    //    "religious-muslim",
    //    "restaurant",
    //    "roadblock",
    //    "rocket",
    //    "school",
    //    "scooter",
    //    "shop",
    //    "skiing",
    //    "slaughterhouse",
    //    "soccer",
    //    "square-stroked",
    //    "square",
    //    "star-stroked",
    //    "star",
    //    "suitcase",
    //    "swimming",
    //    "telephone",
    //    "tennis",
    //    "theatre",
    //    "toilets",
    //    "town-hall",
    //    "town",
    //    "triangle-stroked",
    //    "triangle",
    //    "village",
    //    "warehouse",
    //    "waste-basket",
    //    "water",
    //    "wetland",
    //    "zoo"
    //];
    var availableIcons = {};
    function registerIcon(icon, text) {
        availableIcons[icon] = {
            icon: icon,
            text: text
        };
    }

    registerIcon("star", "Allgemein");
    registerIcon("bicycle", "Fahrrad");
    registerIcon("park", "Park");
    registerIcon("garden", "Park/Blumen");
    registerIcon("museum", "Museum");
    registerIcon("zoo", "Zoo");
    registerIcon("theatre", "Theater");
    registerIcon("art-gallery", "Kunst");
    registerIcon("library", "Bibliothek");
    registerIcon("fast-food", "Imbiss");
    registerIcon("shop", "Shoppen");
    registerIcon("cafe", "Cafe");
    registerIcon("restaurant", "Restaurant");
    registerIcon("bar", "Bar/Kneipe");
    registerIcon("cinema", "Kino");
    registerIcon("music", "Musik");
    registerIcon("bakery", "Bäcker");

    return {
        isIconAvailable: function (iconName) {
            return !!availableIcons[iconName];
        },
        availableIcons: availableIcons
    };
});