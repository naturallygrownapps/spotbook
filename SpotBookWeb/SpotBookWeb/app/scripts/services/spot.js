﻿angular.module("SpotsIn").factory("Spot", function ($resource, sbApiPath) {
    return $resource(sbApiPath.get("/spots?spotbooks=:spotbookid"),
      { }, {
          save: {
              method: "POST",
              url: sbApiPath.get("/spots")
          },
          removespot: {
              method: "DELETE",
              url: sbApiPath.get("/spots/:id")
          }
      }
     );
});