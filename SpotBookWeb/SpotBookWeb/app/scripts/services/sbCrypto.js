﻿angular.module("SpotsIn").factory("sbCrypto", function () {

    function hash(password) {
        return CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);
    }

    return {
        hash: hash
    };
});