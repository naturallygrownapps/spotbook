﻿angular.module("SpotsIn").factory("sbApiPath", function () {
    function get(path) {
        return sbConfig.server + path;
    }

    return {
        get: get
    };
});