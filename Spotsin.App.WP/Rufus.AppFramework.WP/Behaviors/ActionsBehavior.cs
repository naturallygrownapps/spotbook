﻿using Microsoft.Xaml.Interactivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Markup;

namespace Rufus.AppFramework.WP.Behaviors
{
    [ContentProperty(Name = "Actions")]
    public abstract class ActionsBehavior : BehaviorBase
    {
        public ActionCollection Actions
        {
            get { return (ActionCollection)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }

        public static readonly DependencyProperty ActionsProperty =
            DependencyProperty.Register("Actions", typeof(ActionCollection), typeof(ActionsBehavior), new PropertyMetadata(null));

        public ActionsBehavior()
        {
            this.Actions = new ActionCollection();
        }

        public void InvokeActions(object parameter)
        {
            var obj = AssociatedObject;
            if (obj == null)
                return;
            Interaction.ExecuteActions(obj, Actions, parameter);
        }
    }
}
