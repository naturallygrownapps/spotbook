﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Xaml;

namespace Rufus.AppFramework.WP.Behaviors
{
    public class KeyTriggerBehavior : ActionsBehavior
    {
        public VirtualKey Key
        {
            get { return (VirtualKey)GetValue(KeyProperty); }
            set { SetValue(KeyProperty, value); }
        }

        public static readonly DependencyProperty KeyProperty =
            DependencyProperty.Register("Key", typeof(VirtualKey), typeof(KeyTriggerBehavior), new PropertyMetadata(VirtualKey.None));

        public KeyTriggerBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            var uiElem = AssociatedObject as UIElement;
            if (uiElem != null)
            {
                uiElem.KeyDown += OnKeyDown;
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            var uiElem = AssociatedObject as UIElement;
            if (uiElem != null)
            {
                uiElem.KeyDown -= OnKeyDown;
            }
        }

        private void OnKeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.KeyStatus.RepeatCount == 0 && e.Key == Key)
            {
                InvokeActions(null);
            }
        }
    }
}
