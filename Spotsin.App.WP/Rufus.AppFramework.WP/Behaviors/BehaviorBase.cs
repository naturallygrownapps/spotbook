﻿using Microsoft.Xaml.Interactivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Rufus.AppFramework.WP.Behaviors
{
    public abstract class BehaviorBase : DependencyObject, IBehavior
    {
        private DependencyObject _associatedObject;
        public DependencyObject AssociatedObject
        {
            get { return _associatedObject; }
        }

        public void Attach(DependencyObject associatedObject)
        {
            _associatedObject = associatedObject;
            OnAttached();
        }

        protected virtual void OnAttached()
        {
        }

        public void Detach()
        {
            OnDetaching();
            _associatedObject = null;
        }

        protected virtual void OnDetaching()
        {
        }
    }
}
