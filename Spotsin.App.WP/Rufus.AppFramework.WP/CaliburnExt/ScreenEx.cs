﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.WP.CaliburnExt
{
    public class ScreenEx : Screen
    {
        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            var fe = view as Windows.UI.Xaml.FrameworkElement;
            if (fe != null)
            {
                fe.Loaded += OnViewReloadedInternal;
            }
        }

        private void OnViewReloadedInternal(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            OnViewReloaded(sender);
        }

        protected virtual void OnViewReloaded(object view)
        {
        }
    }
}
