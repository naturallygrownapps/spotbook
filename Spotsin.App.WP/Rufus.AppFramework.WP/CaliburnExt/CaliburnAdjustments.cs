﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.WP.CaliburnExt
{
    public static class CaliburnAdjustments
    {
        /// <summary>
        /// Registers a new message event shortcut for keys.
        /// Example usage: Message.Attach="[Key Enter] = [Action DoIt]"
        /// </summary>
        public static void PatchCreateTriggerForKeyEvents()
        {
            var oldCreateTrigger = Parser.CreateTrigger;
            Parser.CreateTrigger = (target, triggerText) =>
            {
                if (triggerText != null && triggerText.StartsWith("[Key "))
                {
                    var keyName = triggerText.Replace("[", string.Empty)
                                             .Replace("]", string.Empty)
                                             .Replace("Key", string.Empty)
                                             .Trim();

                    Windows.System.VirtualKey key;
                    if (!Enum.TryParse<Windows.System.VirtualKey>(keyName, out key))
                    {
                        throw new InvalidOperationException("Could not parse a key from '" + keyName + "'.");
                    }

                    return new Behaviors.KeyTriggerBehavior()
                    {
                        Key = key
                    };
                }
                else
                {
                    return oldCreateTrigger(target, triggerText);
                }
            };
        }
    }
}
