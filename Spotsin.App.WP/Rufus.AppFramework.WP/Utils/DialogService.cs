﻿using Rufus.AppFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace Rufus.AppFramework.WP.Utils
{
    public interface IDialogService
    {
        Task<DialogResult> ShowOkCancelDialogAsync(string message, string title = null);
        Task<DialogResult> ShowOkDialogAsync(string message, string title = null);
    }

    public enum DialogResult
    {
        Ok, Cancel
    }

    class DialogService : IDialogService
    {
        private readonly AsyncLock _syncLock = new AsyncLock();

        public DialogService()
        {
        }

        public async Task<DialogResult> ShowOkCancelDialogAsync(string message, string title = null)
        {
            if (message == null)
                return DialogResult.Cancel;

            MessageDialog dlg;
            if (title == null)
                dlg = new MessageDialog(message);
            else
                dlg = new MessageDialog(message, title);

            var okCmd = new UICommand("Ok");
            var cancelCmd = new UICommand("Abbrechen");
            dlg.Commands.Add(okCmd);
            dlg.Commands.Add(cancelCmd);

            dlg.DefaultCommandIndex = 0;
            dlg.CancelCommandIndex = 1;

            var selectedCommand = await ShowDialogOnUIThread(dlg);
            if (selectedCommand == okCmd)
                return DialogResult.Ok;
            else
                return DialogResult.Cancel;
        }

        public async Task<DialogResult> ShowOkDialogAsync(string message, string title = null)
        {
            if (message == null)
                return DialogResult.Cancel;

            MessageDialog dlg;
            if (title == null)
                dlg = new MessageDialog(message);
            else
                dlg = new MessageDialog(message, title);

            var okCmd = new UICommand("Ok");
            dlg.Commands.Add(okCmd);

            dlg.DefaultCommandIndex = 0;
            dlg.CancelCommandIndex = 0;

            await ShowDialogOnUIThread(dlg);
            return DialogResult.Ok;
        }

        private Task<IUICommand> ShowDialogOnUIThread(MessageDialog dlg)
        {
            var tcs = new TaskCompletionSource<IUICommand>();
            var asyncAction = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                // Only one dialog at a time. More than one dialog causes exceptions to be thrown.
                using (await _syncLock.LockAsync())
                {
                    try
                    {
                        var selectedCommand = await dlg.ShowAsync();
                        tcs.SetResult(selectedCommand);
                    }
                    catch (Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                }
            });
            return tcs.Task;
        }
    }
}
