﻿using Autofac;
using Rufus.AppFramework.WP.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.WP
{
    public class AppFrameworkWPModule : Module
    {
        protected override void Load(Autofac.ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();
        }
    }
}
