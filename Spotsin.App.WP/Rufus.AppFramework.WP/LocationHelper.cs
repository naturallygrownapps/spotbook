﻿using Rufus.AppFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;

namespace Rufus.AppFramework.WP
{
    public class LocationHelper
    {
        public static readonly Geopoint InvalidLocation = new Geopoint(new BasicGeoposition());

        public event TypedEventHandler<LocationHelper, PositionChangedEventArgs> LocationChanged;

        private readonly Geolocator _locator;

        public LocationHelper()
        {
            _locator = new Geolocator();
            _locator.DesiredAccuracy = PositionAccuracy.High;
            _locator.MovementThreshold = 5.0;
            _locator.PositionChanged += OnPositionChanged;
        }

        private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            var handler = this.LocationChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public async Task<Optional<Geopoint>> TryGetCurrentPosition()
        {
            try
            {
                var pos = await _locator.GetGeopositionAsync();
                return pos.Coordinate.Point;
            }
            catch (UnauthorizedAccessException)
            {
                return new Optional<Geopoint>();
            }
        }

        public async Task<Geopoint> GetCurrentPosition()
        {
            try
            {
                var pos = await _locator.GetGeopositionAsync();
                return pos.Coordinate.Point;
            }
            catch (UnauthorizedAccessException)
            {
                return InvalidLocation;
            }
        }
    }
}
