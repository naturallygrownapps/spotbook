﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Rufus.AppFramework.WP.Converters
{
    public abstract class ConverterBase<TFrom, TTo> : IValueConverter
    {
        public ConverterBase()
        {
        }

        public virtual object Convert(object value, Type targetType, object parameter, string language)
        {
            return ConvertTyped((TFrom)value, targetType, parameter, language);
        }

        protected abstract TTo ConvertTyped(TFrom value, Type targetType, object parameter, string language);

        public virtual object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return ConvertBackTyped((TTo)value, targetType, parameter, language);
        }

        protected abstract TFrom ConvertBackTyped(TTo value, Type targetType, object parameter, string language);
    }
}
