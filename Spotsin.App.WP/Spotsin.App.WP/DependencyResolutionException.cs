﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP
{
    public class DependencyResolutionException : Exception
    {
        public DependencyResolutionException()
            : base()
        {
        }

        public DependencyResolutionException(string msg)
            : base(msg)
        {
        }

        public DependencyResolutionException(string msg, Exception inner)
            : base(msg, inner)
        {
        }
    }
}
