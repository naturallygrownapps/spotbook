﻿using Caliburn.Micro;
using Rufus.AppFramework.Utils;
using Rufus.AppFramework.Validation;
using Rufus.AppFramework.WP.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP
{
    public class LoginPageViewModel : Screen
    {
        private string _username;
        [RequiredValue]
        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyOfPropertyChange("Username"); }
        }

        private string _password;
        [RequiredValue]
        public string Password
        {
            get { return _password; }
            set { _password = value; NotifyOfPropertyChange("Password"); }
        }

        private readonly TaskTracker _asyncStatus = new TaskTracker();
        public INotifyBusy AsyncStatus
        {
            get { return _asyncStatus; }
        }

        private readonly ViewModelValidator _validator;
        public ViewModelValidator Validator
        {
            get { return _validator; }
        } 

        private readonly LoginService _loginSvc;
        private readonly INavigationService _navSvc;
        private readonly IDialogService _dlgSvc;
        private readonly MetroLog.ILogger _logger;

        public LoginPageViewModel(LoginService loginSvc, INavigationService navSvc, 
                                  IDialogService dlgSvc, MetroLog.ILogger logger)
        {
            _loginSvc = loginSvc;
            _navSvc = navSvc;
            _dlgSvc = dlgSvc;
            _logger = logger;
            _validator = new ViewModelValidator(this, true);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            // Never go back from the login page.
            _navSvc.BackStack.Clear();
            _navSvc.ForwardStack.Clear();
        }

        public async void Login()
        {
            ExceptionDispatchInfo exInfo = null;
            try
            {
                await _loginSvc.Login(Username, Password).Track(_asyncStatus);
            }
            catch(Exception ex)
            {
                exInfo = ExceptionDispatchInfo.Capture(ex);
            }

            if (exInfo != null)
            {
                _logger.Error(exInfo.SourceException.GetFlattenedMessage(), exInfo.SourceException);
                await _dlgSvc.ShowOkDialogAsync(exInfo.SourceException.GetFlattenedMessage(), "Fehler");
            }
            else
            {
                _navSvc.Navigate<MainPage>();
            }
        }
    }
}
