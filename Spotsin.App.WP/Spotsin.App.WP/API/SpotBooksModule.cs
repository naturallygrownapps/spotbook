﻿using Spotsin.Data.Dto;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP.API
{
    public class SpotBooksModule
    {
        private readonly IRemoteEndpoint _sbEndpoint;

        public SpotBooksModule(APIClient httpClient)
        {
            _sbEndpoint = httpClient.GetEndpoint("/spotbooks");
        }

        public async Task<IEnumerable<SpotBookDto>> GetSpotBooks()
        {
            var response = await _sbEndpoint.GetAsync<SpotBooksModuleParameters.GetSpotBooksOutput>("");
            return response;
        }

        public async Task<SpotBookDto> GetSpotBook(string id)
        {
            var response = await _sbEndpoint.GetAsync<SpotBookDto>(id);
            return response;
        }
    }
}
