﻿using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP.API
{
    public class AuthModule
    {
        private readonly IRemoteEndpoint _authEndpoint;

        public AuthModule(APIClient httpClient)
        {
            _authEndpoint = httpClient.GetEndpoint("/auth");
        }

        public async Task<string> Login(string username, string passwordHash)
        {
            var response = await _authEndpoint.PostAsync<AuthModuleParameters.LoginOutput>("/login", new AuthModuleParameters.LoginInput()
                {
                    Username = username,
                    Password = passwordHash
                });
            return response.Token;
        }

        public async Task Register(string username, string password)
        {
            var response = await _authEndpoint.PostAsync<AuthModuleParameters.RegisterOutput>("/register", new AuthModuleParameters.RegisterInput()
                {
                    Username = username,
                    Password = password
                });
        }
    }
}
