﻿using Caliburn.Micro;
using MetroLog;
using Rufus.AppFramework.Utils;
using Spotsin.App.WP.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spotsin.App.WP.API
{
    public class APIClient
    {
        private class RemoteEndpoint : IRemoteEndpoint
        {
            private readonly APIClient _client;
            private readonly string _endPoint;
            private readonly ILogger _logger;
            private string _baseUri;

            public RemoteEndpoint(APIClient client, string endPoint, ILogger logger)
            {
                _client = client;
                _endPoint = endPoint;
                _logger = logger;
            }

            public Task<T> PostAsync<T>(object content, string path, CancellationToken ct)
            {
                var uri = GetUriFor(path);
                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new StringContent(_client._jsonParser.ToJson(content), Encoding.UTF8, "application/json");
                return SendRequest<T>(request, ct);
            }

            public Task<T> GetAsync<T>(string path, CancellationToken ct)
            {
                var uri = GetUriFor(path);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                return SendRequest<T>(request, ct);
            }

            private async Task<T> SendRequest<T>(HttpRequestMessage request, CancellationToken ct)
            {
                try
                {
                    var response = await _client.Http.SendAsync(request, ct);
                    var responseContent = await response.Content.ReadAsStringAsync();

                    if (!response.IsSuccessStatusCode)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        {
                            _client.NotifyAuthRequired();
                            throw new OperationCanceledException();
                        }

                        try
                        {
                            var error = _client._jsonParser.Parse<Spotsin.Data.APIError>(responseContent);
                            if (error != null)
                            {
                                throw new HttpException(error);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Received failure status code with unparsable error content.", ex);
                        }

                        throw new HttpException(response);
                    }
                    else
                    {
                        var result = _client._jsonParser.Parse<T>(responseContent);
                        return result;
                    }
                }
                catch(HttpRequestException ex)
                {
                    throw new HttpException(ex.Message, ex);
                }
            }

            private Uri GetUriFor(string path)
            {
                if (_baseUri == null)
                {
                    _baseUri = JoinUris(_client.Http.BaseAddress, _endPoint);
                }

                var uri = new Uri(JoinUriLikeStrings(_baseUri, path));
                return uri;
            }

            private string JoinUris(Uri baseUri, string path)
            {
                var baseStr = baseUri.AbsoluteUri;
                return JoinUriLikeStrings(baseStr, path);
            }

            private string JoinUriLikeStrings(string uri1, string uri2)
            {
                if (!uri1.EndsWith("/"))
                {
                    uri1 += "/";
                }
                if (uri2.StartsWith("/"))
                {
                    uri2 = uri2.Remove(0, 1);
                }

                return uri1 + uri2;
            }
        }

        private readonly object _httpInitLock = new object();
        private System.Net.Http.HttpClient _http;
        private System.Net.Http.HttpClient Http
        {
            get { if (_http == null) Initialize(); return _http; }
        }

        private readonly Settings _settings;
        private readonly JsonParser _jsonParser;
        private readonly ILogger _logger;
        private readonly IEventAggregator _evtAggregator;

        public APIClient(Settings settings, JsonParser jsonParser, 
                         ILogger logger, IEventAggregator evtAggregator)
        {
            _settings = settings;
            _jsonParser = jsonParser;
            _logger = logger;
            _evtAggregator = evtAggregator;
        }

        private void Initialize()
        {
            if (_http == null)
            {
                lock (_httpInitLock)
                {
                    if (_http == null)
                    {
                        _http = new System.Net.Http.HttpClient();
                        _http.BaseAddress = new Uri(_settings.ServerBaseAddress);
                        _http.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    }
                }
            }
        }

        private void NotifyAuthRequired()
        {
            var evt = new AuthenticationRequiredEvent();
            _evtAggregator.PublishOnUIThread(evt);
        }

        public void SetAuthToken(string token)
        {
            Http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", token);
        }

        public IRemoteEndpoint GetEndpoint(string endPoint)
        {
            return new RemoteEndpoint(this, endPoint, _logger);
        }
    }
}
