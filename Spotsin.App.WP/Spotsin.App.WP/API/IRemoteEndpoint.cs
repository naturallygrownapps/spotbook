﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spotsin.App.WP.API
{
    public interface IRemoteEndpoint
    {
        Task<T> PostAsync<T>(object body, string path, CancellationToken ct);
        Task<T> GetAsync<T>(string path, CancellationToken ct);
    }

    public static class IRemoteEndpointExtensions
    {
        public static Task<T> PostAsync<T>(this IRemoteEndpoint self, string path, object body)
        {
            return self.PostAsync<T>(body, path, CancellationToken.None);
        }

        public static Task<T> GetAsync<T>(this IRemoteEndpoint self, string path)
        {
            return self.GetAsync<T>(path, CancellationToken.None);
        }
    }
}
