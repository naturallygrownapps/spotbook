﻿using Rufus.AppFramework.Utils;
using Spotsin.Data.Dto;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spotsin.App.WP.API
{
    public class SpotsModule
    {
        private readonly IRemoteEndpoint _spotsEndpoint;

        public SpotsModule(APIClient httpClient)
        {
            _spotsEndpoint = httpClient.GetEndpoint("/spots");
        }

        public async Task<IEnumerable<SpotDto>> GetSpots(IEnumerable<string> spotBooks, 
                                                         double minX, double minY, double maxX, double maxY,
                                                         CancellationToken ct)
        {
            var query = "?spotbooks=" + string.Join(";", spotBooks);
            query += "&bb=" + DoubleParser.ToString(minX) + "," + 
                              DoubleParser.ToString(minY) + "," +
                              DoubleParser.ToString(maxX) + "," +
                              DoubleParser.ToString(maxY);
            var response = await _spotsEndpoint.GetAsync<IEnumerable<SpotDto>>(query, ct);
            return response;
        }
    }
}
