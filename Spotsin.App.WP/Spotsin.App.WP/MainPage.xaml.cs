﻿using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Spotsin.App.WP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MapControl Map
        {
            get { return map; }
        }

        private readonly IRandomAccessStreamReference _spotIcon;

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().SetDesiredBoundsMode(Windows.UI.ViewManagement.ApplicationViewBoundsMode.UseCoreWindow);
            var hideAction = Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();

            map.MapServiceToken = "Ag1oYpUldQJm55SIdzrMRib_NSWHrPxNdQ8KitdHJXRQo01v8Bf1UhrL_RuFAFtu";
            //var tileDataSource = new HttpMapTileDataSource("http://tiles.spotsin.berlin/1.0.0/osm_spotsin/{zoomlevel}/{x}/{y}.png");
            //var tileSource = new MapTileSource(tileDataSource);
            //tileSource.IsFadingEnabled = false;
            //tileSource.Layer = MapTileLayer.BackgroundReplacement;
            //map.TileSources.Add(tileSource);
            //map.Style = MapStyle.None;
            map.PedestrianFeaturesVisible = true;
            map.Center = Spotsin.App.WP.Utils.Settings.DefaultLocation;

            _spotIcon = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Resources/Images/icon_marker.png"));
        }

        public void SetSpots(IEnumerable<SpotDto> spots)
        {
            map.Children.Clear();
            if (spots != null)
            {
                foreach (var s in spots)
                {
                    var pin = new MapPin();
                    pin.DataContext = s;
                    map.Children.Add(pin);
                }
            }
        }

        //public void SetSpots(IEnumerable<SpotDto> spots)
        //{
        //    map.MapElements.Clear();
        //    if (spots != null)
        //    {
        //        foreach (var s in spots)
        //        {
        //            var icon = new MapIcon()
        //            {
        //                Image = _spotIcon,
        //                Location = new Geopoint(new BasicGeoposition()
        //                {
        //                    Longitude = s.Location.Longitude,
        //                    Latitude = s.Location.Latitude
        //                }),
        //                NormalizedAnchorPoint = new Point(0.5, 1.0),
        //                Title = s.Summary
        //            };
        //            map.MapElements.Add(icon);
        //        }
        //    }
        //}

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void OnMapTapped(MapControl sender, MapInputEventArgs args)
        {
            cbSpotBooks.IsDropDownOpen = false;
        }
    }
}
