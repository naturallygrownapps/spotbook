﻿using Rufus.AppFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace Spotsin.App.WP.Utils
{
    public class SettingsKeys
    {
        public const string ServerBaseAddressKey = "serverBaseAddress";
        public const string UsernameKey = "username";
        public const string PreviousLocation = "previousLocation";
    }

    public class Settings : SettingsBase
    {
        public static readonly Geopoint DefaultLocation = new Geopoint(new BasicGeoposition()
        {
            Latitude = 52.518611, 
            Longitude = 13.408333
        });

        public string ServerBaseAddress
        {
            get { return GetValue<string>(SettingsKeys.ServerBaseAddressKey, "https://spotsin.berlin/api/v1"); }
        }

        public string Username
        {
            get { return GetValue<string>(SettingsKeys.UsernameKey); }
            set { SetValue(SettingsKeys.UsernameKey, value); }
        }

        public Geopoint PreviousLocation
        {
            get
            {
                var str = GetValue<string>(SettingsKeys.PreviousLocation);
                if (!string.IsNullOrEmpty(str))
                {
                    var split = str.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (split.Length == 3)
                    {
                        // lat;lon
                        var lat = DoubleParser.Parse(split[0]);
                        var lon = DoubleParser.Parse(split[1]);
                        if (lat.HasValue && lon.HasValue)
                        {
                            return new Geopoint(new BasicGeoposition()
                                {
                                    Longitude = lon.Value,
                                    Latitude = lat.Value
                                });
                        }
                    }
                }
                return DefaultLocation;
            }
            set
            {
                var strLocation = DoubleParser.ToString(value.Position.Longitude) + ";" + DoubleParser.ToString(value.Position.Latitude);
                SetValue(SettingsKeys.PreviousLocation, strLocation);
            }
        }
    }
}
