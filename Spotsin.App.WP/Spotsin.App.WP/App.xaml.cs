﻿using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Caliburn.Micro;
using Spotsin.App.WP.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Rufus.AppFramework.AutofacExt;
using Rufus.AppFramework.CaliburnExt;
using Rufus.AppFramework.Utils;
using Spotsin.App.WP.API;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Spotsin.App.WP
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : CaliburnApplication
    {
        private readonly CaliburnLogHelper _caliburnLogHelper;

        private FrameAdapter _rootFrameAdapter;
        private IContainer _container;

        public App()
        {
            //MetroLog.GlobalCrashHandler.Configure();
            UnhandledException += OnUnhandledApplicationException;
            System.Threading.Tasks.TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

            // Caliburn statically assigns a null-log at the first access so we have to come first with our log to get all messages.
            _caliburnLogHelper = new CaliburnLogHelper();
            Caliburn.Micro.LogManager.GetLog = _caliburnLogHelper.LogForType;

            SettingsStorage.SetStorageHandlers(
                delegate(string key, out object value)
                {
                    return Windows.Storage.ApplicationData.Current.LocalSettings.Values.TryGetValue(key, out value);
                },
                delegate(string key, object value)
                {
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values[key] = value;
                });
        }

        private void OnUnobservedTaskException(object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception.ContainsInnerException(typeof(OperationCanceledException)))
            {
                e.SetObserved();
            }
        }

        private void OnUnhandledApplicationException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.Exception is OperationCanceledException)
            {
                e.Handled = true;
            }
        }

        protected override void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new Rufus.AppFramework.WP.AppFrameworkWPModule());
            builder.RegisterModule(new Rufus.AppFramework.AppFrameworkModule());

            builder.Register(_ => _rootFrameAdapter).As<INavigationService>().SingleInstance();
            builder.RegisterInstance(MetroLog.LogManagerFactory.DefaultLogManager).As<MetroLog.ILogManager>();
            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

            RegisterDefaultSingleton<APIClient>(builder);
            RegisterDefaultSingleton<Settings>(builder);
            RegisterDefaultSingleton<LoginService>(builder);
            RegisterDefaultSingleton<AuthModule>(builder);
            RegisterDefaultSingleton<SpotBooksModule>(builder);
            RegisterDefaultSingleton<SpotsModule>(builder);
            RegisterDefaultSingleton<SpotsManager>(builder);

            RegisterDefaultSingleton<MainPageViewModel>(builder);
            RegisterDefaultSingleton<LoginPageViewModel>(builder);

            _container = builder.Build();

            _caliburnLogHelper.SetLogManager(_container.Resolve<MetroLog.ILogManager>());
            Rufus.AppFramework.WP.CaliburnExt.CaliburnAdjustments.PatchCreateTriggerForKeyEvents();
        }

        private IRegistrationBuilder<T, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterDefaultSingleton<T>(ContainerBuilder builder)
        {
            return builder.RegisterType<T>().AsSelf().SingleInstance().AutoConfigureLogger();
        }

        protected override object GetInstance(Type service, string key)
        {
            try
            {
                if (service == null)
                    return null;

                if (string.IsNullOrWhiteSpace(key))
                    return _container.Resolve(service);
                else
                    return _container.ResolveKeyed(key, service);
            }
            catch (Exception ex)
            {
                if (ex is Autofac.Core.Registration.ComponentNotRegisteredException ||
                    ex is Autofac.Core.DependencyResolutionException)
                {
                    var msg = "Could not resolve dependency for type '" + service.Name + "'";
                    if (!string.IsNullOrWhiteSpace(key))
                        msg += " with key '" + key + "'";
                    throw new DependencyResolutionException(msg, ex);
                }

                throw;
            }
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.Resolve(typeof(IEnumerable<>).MakeGenericType(service)) as IEnumerable<object>;
        }

        protected override void BuildUp(object instance)
        {
            _container.InjectProperties(instance);
        }

        protected override void PrepareViewFirst(Windows.UI.Xaml.Controls.Frame rootFrame)
        {
            _rootFrameAdapter = new FrameAdapter(rootFrame);
            base.PrepareViewFirst(rootFrame);
        }

        protected override void OnLaunched(Windows.ApplicationModel.Activation.LaunchActivatedEventArgs args)
        {
            if (args.PreviousExecutionState == Windows.ApplicationModel.Activation.ApplicationExecutionState.Running)
            {
                return;
            }

            DisplayRootView<MainPage>();
        }

        protected override void OnSuspending(object sender, SuspendingEventArgs e)
        {
            base.OnSuspending(sender, e);
        }
    }
}