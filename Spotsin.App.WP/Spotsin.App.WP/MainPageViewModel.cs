﻿using Caliburn.Micro;
using Rufus.AppFramework.CaliburnExt;
using Rufus.AppFramework.Utils;
using Rufus.AppFramework.WP;
using Rufus.AppFramework.WP.CaliburnExt;
using Spotsin.App.WP.API;
using Spotsin.App.WP.Utils;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace Spotsin.App.WP
{
    public class MainPageViewModel : ScreenEx, IHandle<AuthenticationRequiredEvent>
    {
        private readonly TaskTracker _asyncStatus = new TaskTracker();
        public INotifyBusy AsyncStatus
        {
            get { return _asyncStatus; }
        }

        private SpotBookDto _currentSpotBook;
        public SpotBookDto CurrentSpotBook
        {
            get { return _currentSpotBook; }
            set 
            {
                if (_currentSpotBook == value)
                    return;
                _currentSpotBook = value; 
                NotifyOfPropertyChange("CurrentSpotBook"); 
                OnCurrentSpotBookChanged(value); 
            }
        }

        private readonly UpdatableCollection<SpotBookDto> _spotBooks = new UpdatableCollection<SpotBookDto>();
        public IEnumerable<SpotBookDto> SpotBooks
        {
            get { return _spotBooks; }
        }

        private readonly Dictionary<string, SpotDto> _spotsById = new Dictionary<string, SpotDto>();
        private readonly UpdatableCollection<SpotDto> _spots = new UpdatableCollection<SpotDto>();
        public IEnumerable<SpotDto> Spots
        {
            get { return _spots; }
        }

        private MainPage _view;
        private MapLoadingTaskConverter _mapLoadingTracker;
        private Geopoint _lastPos;
        private bool _obtainingLocation = false;

        private readonly LocationHelper _locationHelper;
        private readonly LoginService _loginSvc;
        private readonly Settings _settings;
        private readonly SpotBooksModule _sbModule;
        private readonly SpotsManager _spotsManager;
        private readonly IEventAggregator _evtAggregator;
        private readonly INavigationService _navSvc;

        public MainPageViewModel(LoginService loginSvc, Settings settings, 
                                 SpotBooksModule sbModule, SpotsManager spotsManager,
                                 IEventAggregator evtAggregator, INavigationService navSvc)
        {
            _loginSvc = loginSvc;
            _settings = settings;
            _sbModule = sbModule;
            _spotsManager = spotsManager;
            _evtAggregator = evtAggregator;
            _evtAggregator.Subscribe(this);
            _navSvc = navSvc;
            _locationHelper = new LocationHelper();

            Windows.UI.Xaml.Application.Current.Suspending += OnAppSuspending;
        }

        private void OnAppSuspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            if (_view != null)
            {
                try
                {
                    _settings.PreviousLocation = _view.Map.Center;
                }
                catch { }
            }
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            _view = view as MainPage;
            InitMap();
            await _loginSvc.RequireLogin().Track(_asyncStatus);
            await InitSpots();
        }

        protected override async void OnViewReloaded(object view)
        {
            await _loginSvc.RequireLogin().Track(_asyncStatus);
            await InitSpots();
        }

        private async Task InitSpots()
        {
            await ReloadSpotBooks().Track(_asyncStatus);
            // TODO: Select previously selected SpotBook.
            CurrentSpotBook = SpotBooks.FirstOrDefault();
            await LoadSpots().Track(_asyncStatus);
        }

        private async void InitMap()
        {
            //_mapLoadingTracker = new MapLoadingTaskConverter(_view.Map, _asyncStatus);

            await _view.Map.TrySetViewAsync(_settings.PreviousLocation);
            _view.Map.LoadingStatusChanged += OnMapLoadingStatusChanged;
            //_view.Map.CenterChanged += OnMapCenterChanged;

            //var userPos = await _locationHelper.TryGetCurrentPosition();
            //if (userPos.HasValue)
            //{
            //    await _view.Map.TrySetViewAsync(userPos.Value);
            //}
        }

        private async void OnCurrentSpotBookChanged(SpotBookDto newSpotBook)
        {
            await LoadSpots().Track(_asyncStatus);
        }

        private async void OnMapLoadingStatusChanged(Windows.UI.Xaml.Controls.Maps.MapControl sender, object args)
        {
            if (sender.LoadingStatus != Windows.UI.Xaml.Controls.Maps.MapLoadingStatus.Loaded)
            {
                return;
            }

            var pos = sender.Center;
            bool update = false;
            if (_lastPos == null)
            {
                update = true;
            }
            else
            {
                var dist = GeoUtils.DistanceInMetres(pos.Position, _lastPos.Position);
                update = dist > 250;
            }

            if (update)
            {
                await LoadSpots().Track(_asyncStatus);
            }
        }

        private async Task LoadSpots()
        {
            try
            {
                var currentSb = CurrentSpotBook;
                if (currentSb == null || _view == null)
                    return;
                var map = _view.Map;

                var center = map.Center;
                _lastPos = center;
                var mapWidthPx = map.ActualWidth;
                var mapHeightPx = map.ActualHeight;
                // Increase the range so that we load a bit more than we see.
                var increase = 0.25;
                Geopoint topLeft, bottomRight;
                map.GetLocationFromOffset(new Windows.Foundation.Point(-mapWidthPx * increase, -mapHeightPx * increase), out topLeft);
                map.GetLocationFromOffset(new Windows.Foundation.Point(mapWidthPx + mapWidthPx * increase,
                                                                       mapHeightPx + mapHeightPx * increase), out bottomRight);

                IEnumerable<SpotDto> spots = null;
                try
                {
                    spots = await _spotsManager.GetSpotsInBB(currentSb.Id,
                                                             topLeft.Position.Longitude,
                                                             topLeft.Position.Latitude,
                                                             bottomRight.Position.Longitude,
                                                             bottomRight.Position.Latitude);
                }
                catch (OperationCanceledException)
                {
                    return;
                }

                _view.SetSpots(spots);
                //foreach (var s in spots)
                //{
                //    if (_spotsById.ContainsKey(s.Id))
                //    {
                //        continue;
                //    }
                //    else
                //    {
                //        _spots.Add(s);
                //        _spotsById[s.Id] = s;
                //    }
                //}
            }
            catch
            {
                // Ensure we update the next time the map moves.
                _lastPos = null;
                throw;
            }
        }

        private async Task ReloadSpotBooks()
        {
            var foundSpotBooks = await _sbModule.GetSpotBooks();
            _spotBooks.Update(foundSpotBooks);
        }

        public async void GoToMyLocation()
        {
            await GoToMyLocationAsync().Track(_asyncStatus);
        }

        private async Task GoToMyLocationAsync()
        {
            if (_obtainingLocation)
                return;

            _obtainingLocation = true;
            try
            {
                var userPos = await _locationHelper.TryGetCurrentPosition();
                if (userPos.HasValue)
                {
                    await _view.Map.TrySetViewAsync(userPos.Value);
                }
            }
            finally
            {
                _obtainingLocation = false;
            }
        }

        public void Handle(AuthenticationRequiredEvent message)
        {
            _navSvc.Navigate<LoginPage>();
        }
    }
}
