﻿using Rufus.AppFramework.Utils;
using Spotsin.App.WP.API;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spotsin.App.WP
{
    public class SpotsManager
    {
        private struct TileCoordinates
        {
            public int X;
            public int Y;

            public string GetId()
            {
                return X.ToString() + "," + Y.ToString();
            }

            public static TileCoordinates FromSpot(SpotDto spot)
            {
                var tileX = SpotsManager.Long2Tile(spot.Location.Longitude);
                var tileY = SpotsManager.Lat2Tile(spot.Location.Latitude);
                return new TileCoordinates()
                {
                    X = tileX,
                    Y = tileY
                };
            }
        }

        private struct BoundingBox
        {
            public double MinX;
            public double MinY;
            public double MaxX;
            public double MaxY;
        }

        private class SpotTile
        {
            private List<SpotDto> _spots = new List<SpotDto>();
            public List<SpotDto> Spots
            {
                get { return _spots; }
                set { _spots = value; }
            }

            private DateTime _lastUpdate;
            public DateTime LastUpdate
            {
                get { return _lastUpdate; }
                set { _lastUpdate = value; }
            }
        }

        private class SpotCache
        {
            /// <summary>
            /// spot tile id (x,y) -> SpotTile (spots on this tile)
            /// </summary>
            private readonly Dictionary<string, SpotTile> _spotTiles = new Dictionary<string, SpotTile>();
            /// <summary>
            /// Cache entries are cleared after this period.
            /// </summary>
            private readonly TimeSpan CachePeriod = TimeSpan.FromSeconds(30);

            public SpotTile GetTileForSpot(SpotDto spot)
            {
                var tileCoords = TileCoordinates.FromSpot(spot);
                var tile = GetSpotTile(tileCoords);
                return tile;
            }

            public bool HasSpotTile(TileCoordinates tileCoords)
            {
                return _spotTiles.ContainsKey(tileCoords.GetId());
            }

            private SpotTile GetSpotTile(TileCoordinates tileCoords)
            {
                var id = tileCoords.GetId();
                SpotTile tile;
                if (!_spotTiles.TryGetValue(id, out tile))
                {
                    tile = new SpotTile();
                    _spotTiles[id] = tile;
                }
                return tile;
            }

            public void RemoveSpotTile(TileCoordinates tileCoords)
            {
                _spotTiles.Remove(tileCoords.GetId());
            }

            public void ClearTiles(IEnumerable<TileCoordinates> tileCoordList)
            {
                foreach(var tc in tileCoordList)
                {
                    var tile = GetSpotTile(tc);
                    tile.Spots = new List<SpotDto>();
                    // Force the tile to be updated on the next update.
                    tile.LastUpdate = DateTime.MinValue;
                }
            }

            public void StoreSpot(SpotDto spot)
            {
                var tile = GetTileForSpot(spot);
                tile.Spots.Add(spot);
                tile.LastUpdate = DateTime.Now;
            }

            public void DeleteSpot(SpotDto spot)
            {
                var tile = GetTileForSpot(spot);
                tile.Spots.Remove(spot);
                tile.LastUpdate = DateTime.Now;
            }

            public IEnumerable<SpotDto> GetSpots(IEnumerable<TileCoordinates> tileCoordList)
            {
                List<SpotDto> result = new List<SpotDto>();
                foreach(var tc in tileCoordList)
                {
                    var tile = GetSpotTile(tc);
                    result.AddRange(tile.Spots);
                }
                return result;
            }

            /// <summary>
            /// Removes cache entries that have become too old.
            /// </summary>
            /// <param name="tileCoords">The tile to check.</param>
            public void CheckSpotTile(TileCoordinates tileCoords)
            {
                if (HasSpotTile(tileCoords))
                {
                    var spotTile = GetSpotTile(tileCoords);
                    var diff = DateTime.Now - spotTile.LastUpdate;
                    if (diff > CachePeriod)
                    {
                        // This entry is too old, forget it.
                        RemoveSpotTile(tileCoords);
                    }
                }
            }
        }

        public const int ZoomLevel = 14;
        private readonly Dictionary<string, SpotCache> _caches = new Dictionary<string, SpotCache>();
        private readonly SingleCancellationSource _canceller = new SingleCancellationSource();
        private readonly SpotsModule _spotsModule;
        private readonly AsyncLock _asyncLock = new AsyncLock();

        public SpotsManager(SpotsModule spotsModule)
        {
            _spotsModule = spotsModule;
        }

        private static int Long2Tile(double lon, int zoom = ZoomLevel)
        {
            return (int)(Math.Floor((lon + 180.0) / 360.0 * Math.Pow(2, zoom)));
        }

        private static int Lat2Tile(double lat, int zoom = ZoomLevel)
        {
            return (int)(Math.Floor((1.0 - Math.Log(Math.Tan(lat * Math.PI / 180.0) + 1 / Math.Cos(lat * Math.PI / 180.0)) / Math.PI) / 2 * Math.Pow(2, zoom)));
        }

        private static double Tile2Long(double x, int zoom = ZoomLevel)
        {
            return (x / Math.Pow(2, zoom) * 360.0 - 180.0);
        }

        private static double Tile2Lat(double y, int zoom = ZoomLevel)
        {
            var n = Math.PI - 2 * Math.PI * y / Math.Pow(2, zoom);
            return (180 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n))));
        }

        private SpotCache GetCache(string spotBookId)
        {
            SpotCache cache;
            if (!_caches.TryGetValue(spotBookId, out cache))
            {
                cache = new SpotCache();
                _caches[spotBookId] = cache;
            }
            return cache;
        }

        private BoundingBox GetBoundingBoxLatLon(IEnumerable<TileCoordinates> tileCoordList)
        {
            var minX = double.MaxValue;
            var minY = double.MaxValue;
            var maxX = 0.0;
            var maxY = 0.0;

            foreach(var t in tileCoordList)
            {
                if (t.X < minX)
                {
                    minX = t.X;
                }
                else if (t.X > maxX)
                {
                    maxX = t.X;
                }

                if (t.Y < minY)
                {
                    minY = t.Y;
                }
                else if (t.Y > maxY)
                {
                    maxY = t.Y;
                }
            }

            return new BoundingBox()
            {
                MinX = Tile2Long(minX),
                MinY = Tile2Lat(minY),
                MaxX = Tile2Long(maxX),
                MaxY = Tile2Lat(maxY)
            };
        }

        /// <summary>
        /// Gets all spots in a specified bounding box.
        /// </summary>
        /// <exception cref="OperationCanceledException">Subsequent calls cancel other running threads on this method. An <see cref="OperationCancelledException"/> is thrown in this case.</exception>
        public async Task<IEnumerable<SpotDto>> GetSpotsInBB(string spotBookId, double left, double top, double right, double bottom)
        {
            var ct = _canceller.Start();
            using (await _asyncLock.LockAsync())
            {
                var tileLeft = Long2Tile(left);
                var tileTop = Lat2Tile(top);
                var tileRight = Long2Tile(right);
                var tileBottom = Lat2Tile(bottom);

                var width = tileRight - tileLeft;
                var height = tileBottom - tileTop;

                var cache = GetCache(spotBookId);

                // Determine the set of spot tiles that we need to get from the server.
                var newTileCoords = new List<TileCoordinates>();
                var existingTileCoords = new List<TileCoordinates>();

                for (int x = 0; x <= width; x++)
                {
                    for (int y = 0; y <= height; y++)
                    {
                        var tileCoords = new TileCoordinates() { X = tileLeft + x, Y = tileTop + y };
                        cache.CheckSpotTile(tileCoords);

                        if (!cache.HasSpotTile(tileCoords))
                        {
                            newTileCoords.Add(tileCoords);
                        }
                        else
                        {
                            existingTileCoords.Add(tileCoords);
                        }
                    }
                }

                // Check if we need to get new spots first.
                if (newTileCoords.Any())
                {
                    var bb = GetBoundingBoxLatLon(newTileCoords);
                    var receivedSpots = await _spotsModule.GetSpots(new[] { spotBookId },
                                                                    bb.MinX, bb.MinY, bb.MaxX, bb.MaxY, CancellationToken.None);
                    ct.ThrowIfCancellationRequested();
                    // Clear the tiles that we requested because they may have deleted spots.
                    cache.ClearTiles(newTileCoords);

                    // Store the new spots.
                    foreach (var receivedSpot in receivedSpots)
                    {
                        cache.StoreSpot(receivedSpot);
                    }

                    // Return the new and previously cached spots.
                    var spots = cache.GetSpots(existingTileCoords.Concat(newTileCoords));
                    return spots;
                }
                // Or if we can just return all requested spots because we have them cached.
                else
                {
                    // We have all tiles up to date. Gather the results.
                    var spots = cache.GetSpots(existingTileCoords);
                    return spots;
                }
            }
        }

        public void Clear()
        {
            _caches.Clear();
            _canceller.Cancel();
        }

        public void InvalidateTileForSpot(SpotDto spot)
        {
            var cache = GetCache(spot.SpotBookId);
            var tileCoords = TileCoordinates.FromSpot(spot);
            cache.ClearTiles(new[] { tileCoords });
        }
    }
}
