﻿using Spotsin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP
{
    public class HttpException : Exception
    {
        public HttpException() { }
        public HttpException(string message) : base(message) { }
        public HttpException(string message, Exception inner) : base(message, inner) { }

        private readonly APIError _apiError;

        public HttpException(APIError apiError)
            : base(apiError.ErrorMessage)
        {
            _apiError = apiError;
        }

        public HttpException(HttpResponseMessage msg)
            : base(msg.ReasonPhrase + " (" + ((int)(msg.StatusCode)).ToString() + ")")
        {
        }

        public override string ToString()
        {
            if (_apiError != null)
            {
                return "The remote server returned an error.\n" +
                       "Message: " + _apiError.ErrorMessage + "\n" +
                       "ErrorCode: " + _apiError.ErrorCode + "\n" +
                       "DetailedMessage: " + _apiError.FullException;
            }
            else
            {
                return base.ToString();
            }
        }
    }
}
