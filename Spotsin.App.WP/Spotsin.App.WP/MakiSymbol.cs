﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Spotsin.App.WP
{
    public class MakiSymbol : FontIcon
    {
        public MakiSymbols Symbol
        {
            get { return (MakiSymbols)GetValue(SymbolProperty); }
            set { SetValue(SymbolProperty, value); }
        }

        public static readonly DependencyProperty SymbolProperty =
            DependencyProperty.Register("Symbol", typeof(MakiSymbols), typeof(MakiSymbol), new PropertyMetadata(MakiSymbols.Triangle, OnSymbolChanged));

        private static void OnSymbolChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            ((MakiSymbol)sender).OnSymbolChanged((MakiSymbols)args.NewValue);
        }

        public MakiSymbol()
        {
            FontFamily = new Windows.UI.Xaml.Media.FontFamily("/Resources/Fonts/maki.ttf#maki");
            OnSymbolChanged(Symbol);
        }

        private void OnSymbolChanged(MakiSymbols newValue)
        {
            this.Glyph = ((char)newValue).ToString();
        }
    }

    public enum MakiSymbols
    {
        Triangle = 0xe800,
        TriangleStroked = 0xe801,
        Village = 0xe802,
        Warehouse = 0xe803,
        WasteBasket = 0xe804,
        Water = 0xe805,
        Wetland = 0xe806,
        Zoo = 0xe807,
        Airfield = 0xe808,
        Airport = 0xe809,
        AlcoholShop = 0xe80a,
        ArtGallery = 0xe80b,
        Bakery = 0xe80c,
        Bank = 0xe80d,
        Bar = 0xe80e,
        Beer = 0xe80f,
        Bicycle = 0xe810,
        Building = 0xe811,
        Bus = 0xe812,
        Cafe = 0xe813,
        Camera = 0xe814,
        Campsite = 0xe815,
        Car = 0xe816,
        Cemetery = 0xe817,
        Cinema = 0xe818,
        Circle = 0xe819,
        CircleStroked = 0xe81a,
        City = 0xe81b,
        ClothingStore = 0xe81c,
        College = 0xe81d, 
        Commercial = 0xe81e, 
        Cross = 0xe81f, 
        Dam = 0xe820, 
        Danger = 0xe821, 
        Disability = 0xe822, 
        DogPark = 0xe823, 
        Embassy = 0xe824, 
        EmergencyTelephone = 0xe825, 
        Entrance = 0xe826, 
        Farm = 0xe827, 
        FastFood = 0xe828, 
        Ferry = 0xe829, 
        FireStation = 0xe82a, 
        Fuel = 0xe82b, 
        Garden = 0xe82c, 
        Golf = 0xe82d, 
        Grocery = 0xe82e, 
        Hairdresser = 0xe82f, 
        Harbor = 0xe830, 
        Heart = 0xe831, 
        Heliport = 0xe832, 
        Hospital = 0xe833, 
        Industrial = 0xe834, 
        LandUse = 0xe835, 
        Laundry = 0xe836, 
        Library = 0xe837, 
        Lighthouse = 0xe838, 
        Lodging = 0xe839, 
        Logging = 0xe83a, 
        LondonUnderground = 0xe83b, 
        Marker = 0xe83c, 
        MarkerStroked = 0xe83d, 
        Minefield = 0xe83e, 
        Mobilephone = 0xe83f, 
        Monument = 0xe840, 
        Museum = 0xe841, 
        Music = 0xe842, 
        OilWell = 0xe843, 
        Park = 0xe844, 
        Park2 = 0xe845, 
        Parking = 0xe846, 
        ParkingGarage = 0xe847, 
        Pharmacy = 0xe848, 
        Pitch = 0xe849, 
        PlaceOfWorship = 0xe84a, 
        Playground = 0xe84b, 
        Police = 0xe84c, 
        PollingPlace = 0xe84d, 
        Post = 0xe84e, 
        Prison = 0xe84f, 
        Rail = 0xe850, 
        RailAbove = 0xe851, 
        RailLight = 0xe852, 
        RailMetro = 0xe853, 
        RailUnderground = 0xe854, 
        ReligiousChristian = 0xe855, 
        ReligiousJewish = 0xe856, 
        ReligiousMuslim = 0xe857, 
        Restaurant = 0xe858, 
        Roadblock = 0xe859, 
        Rocket = 0xe85a, 
        School = 0xe85b, 
        Scooter = 0xe85c, 
        Shop = 0xe85d, 
        Skiing = 0xe85e, 
        Slaughterhouse = 0xe85f, 
        Soccer = 0xe860, 
        Square = 0xe861, 
        SquareStroked = 0xe862, 
        Star = 0xe863, 
        StarStroked = 0xe864, 
        Suitcase = 0xe865, 
        Swimming = 0xe866, 
        Telephone = 0xe867, 
        Tennis = 0xe868, 
        Theatre = 0xe869, 
        Toilets = 0xe86a, 
        Town = 0xe86b, 
        TownHall = 0xe86c, 
    }
}
