﻿using Rufus.AppFramework.WP.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.App.WP.Converters
{
    public class MakiSymbolConverter : ConverterBase<string, MakiSymbols>
    {
        protected override MakiSymbols ConvertTyped(string value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return MakiSymbols.Star;
            var v = value.Replace("-", "");
            MakiSymbols result;
            if (!Enum.TryParse<MakiSymbols>(v, true, out result))
            {
                result = MakiSymbols.Star;
            }
            return result;
        }

        protected override string ConvertBackTyped(MakiSymbols value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
