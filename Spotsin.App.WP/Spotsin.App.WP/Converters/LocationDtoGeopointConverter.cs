﻿using Rufus.AppFramework.WP.Converters;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace Spotsin.App.WP.Converters
{
    public class LocationDtoGeopointConverter : ConverterBase<LocationDto, Geopoint>
    {
        protected override Geopoint ConvertTyped(LocationDto value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            return new Geopoint(new BasicGeoposition()
            {
                Longitude = value.Longitude,
                Latitude = value.Latitude
            });
        }

        protected override LocationDto ConvertBackTyped(Geopoint value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            return new LocationDto()
            {
                Longitude = value.Position.Longitude,
                Latitude = value.Position.Latitude
            };
        }
    }
}
