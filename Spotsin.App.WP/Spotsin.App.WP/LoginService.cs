﻿using Caliburn.Micro;
using Rufus.AppFramework.Utils;
using Spotsin.App.WP.API;
using Spotsin.App.WP.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace Spotsin.App.WP
{
    public class LoginService : PropertyChangedBase
    {
        private class SessionState
        {
            private string _token;
            public string Token
            {
                get { return _token; }
                set { _token = value; }
            }
        }

        private bool _isLoggedIn = false;
        public bool IsLoggedIn
        {
            get { return !string.IsNullOrEmpty(_session.Token) && _isLoggedIn; }
        }

        private readonly AuthModule _authModule;
        private readonly APIClient _httpClient;
        private readonly Settings _settings;
        private readonly INavigationService _navSvc;
        private readonly SessionState _session = new SessionState();
        private readonly Windows.Security.Credentials.PasswordVault _pwVault = new Windows.Security.Credentials.PasswordVault();
        private readonly CryptographicHash _sha256;

        private const string PasswordId = "SpotsinPassword";

        public LoginService(AuthModule authModule, APIClient httpClient, 
                            INavigationService navSvc, Settings settings)
        {
            _authModule = authModule;
            _httpClient = httpClient;
            _navSvc = navSvc;
            _settings = settings;
            _sha256 = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256).CreateHash();
            _session.Token = _settings.GetValue<string>("ServiceToken");
        }

        public async Task RequireLogin()
        {
            if (!IsLoggedIn)
            {
                if (!string.IsNullOrEmpty(_session.Token))
                {
                    _httpClient.SetAuthToken(_session.Token);
                    // TODO: Check if token is still valid.
                    _isLoggedIn = true;
                }
                else
                {
                    var existingCredential = LoadCredentials();
                    if (existingCredential != null)
                    {
                        // We already have credentials. Do a relogin.
                        existingCredential.RetrievePassword();
                        await Login(existingCredential.UserName, existingCredential.Password);
                    }
                    else
                    {
                        // No credentials yet - go to the login page.
                        var success = _navSvc.Navigate<LoginPage>();
                    }
                }
            }
        }

        public async Task Login(string username, string password)
        {
            var passwordHash = Sha256(password);
            _session.Token = await _authModule.Login(username, passwordHash);
            _settings.SetValue("ServiceToken", _session.Token);
            _httpClient.SetAuthToken(_session.Token);
            _isLoggedIn = true;
            SaveCredentials(username, password);
        }

        private string Sha256(string msg)
        {
            IBuffer buffUtf8Msg = CryptographicBuffer.ConvertStringToBinary(msg, BinaryStringEncoding.Utf8);
            _sha256.Append(buffUtf8Msg);
            IBuffer buffHash = _sha256.GetValueAndReset();
            return CryptographicBuffer.EncodeToHexString(buffHash);
        }

        private void SaveCredentials(string username, string password)
        {
            _settings.Username = username;
            var pw = new Windows.Security.Credentials.PasswordCredential(PasswordId, username, password);
            _pwVault.Add(pw);
        }

        private Windows.Security.Credentials.PasswordCredential LoadCredentials()
        {
            try
            {
                var username = _settings.Username;
                if (string.IsNullOrEmpty(username))
                    return null;

                return _pwVault.Retrieve(PasswordId, username);
            }
            catch
            {
                return null;
            }
        }
    }
}
