﻿using Rufus.AppFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls.Maps;

namespace Spotsin.App.WP
{
    public class MapLoadingTaskConverter: IDisposable
    {
        private readonly MapControl _map;
        private readonly TaskTracker _tracker;
        private TaskCompletionSource<object> _currentTcs;

        public MapLoadingTaskConverter(MapControl map, TaskTracker tracker)
        {
            _map = map;
            _tracker = tracker;
            _map.LoadingStatusChanged += OnLoadingStatusChanged;
            OnStatusUpdated(_map);
        }

        private void OnLoadingStatusChanged(MapControl sender, object args)
        {
            OnStatusUpdated(sender);
        }

        private void OnStatusUpdated(MapControl map)
        {
            if (map.LoadingStatus == MapLoadingStatus.Loading)
            {
                StartLoading();
            }
            else
            {
                FinishLoading();
            }
        }

        private void StartLoading()
        {
            FinishLoading();
            _currentTcs = new TaskCompletionSource<object>();
            _tracker.TrackTask(_currentTcs.Task);
        }

        private void FinishLoading()
        {
            if (_currentTcs != null)
            {
                _currentTcs.TrySetResult(null);
                _currentTcs = null;
            }
        }

        public void Dispose()
        {
            _map.LoadingStatusChanged -= OnLoadingStatusChanged;
            FinishLoading();
        }
    }
}
