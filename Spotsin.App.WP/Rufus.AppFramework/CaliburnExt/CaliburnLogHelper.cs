﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.CaliburnExt
{
    public class CaliburnLogHelper
    {
        private class CaliburnLog : ILog
        {
            private MetroLog.ILogger _logger;
            public MetroLog.ILogger Logger
            {
                get { return _logger; }
                set { _logger = value; }
            }

            private readonly CaliburnLogHelper _caliburnLogHelper;

            public CaliburnLog(MetroLog.ILogger logger, CaliburnLogHelper caliburnLogHelper)
            {
                _logger = logger;
                _caliburnLogHelper = caliburnLogHelper;
            }

            public void Error(Exception exception)
            {
                if (_logger != null)
                    _logger.Error(exception.Message, exception);
            }

            public void Info(string format, params object[] args)
            {
                if (_logger != null && _caliburnLogHelper.IsInfoEnabled)
                    _logger.Info(string.Format(format, args));
            }

            public void Warn(string format, params object[] args)
            {
                if (_logger != null)
                    _logger.Warn(string.Format(format, args));
            }
        }

        private bool _isInfoEnabled = false;
        public bool IsInfoEnabled
        {
            get { return _isInfoEnabled; }
            set { _isInfoEnabled = value; }
        }

        private readonly Dictionary<Type, CaliburnLog> _logs = new Dictionary<Type, CaliburnLog>();
        private MetroLog.ILogManager _logManager;

        public CaliburnLogHelper()
        {
        }

        public void SetLogManager(MetroLog.ILogManager logManager)
        {
            _logManager = logManager;
            UpdateLogs(logManager);
        }

        private void UpdateLogs(MetroLog.ILogManager newLogManager)
        {
            // Create a new logger if our repository changes. This happens when we
            // define it during dependency configuration.
            foreach (var kv in _logs)
            {
                kv.Value.Logger = CreateLoggerForType(kv.Key, newLogManager);
            }
        }

        public ILog LogForType(Type type)
        {
            CaliburnLog result;
            if (!_logs.TryGetValue(type, out result))
            {
                var cgmLogger = CreateLoggerForType(type, _logManager);
                result = new CaliburnLog(cgmLogger, this);
                _logs[type] = result;
            }
            return result;
        }

        private static MetroLog.ILogger CreateLoggerForType(Type type, MetroLog.ILogManager logManager)
        {
            var cgmLogger = logManager != null ? logManager.GetLogger(type) : null;
            return cgmLogger;
        }
    }
}
