﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.CaliburnExt
{
    public class UpdatableCollection<T> : BindableCollection<T>
    {
        public void Update(IEnumerable<T> newItems)
        {
            this.IsNotifying = false;
            try
            {
                this.Clear();
                this.AddRange(newItems);
            }
            finally
            {
                this.IsNotifying = true;
            }
            this.Refresh();
        }
    }
}
