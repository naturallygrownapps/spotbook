﻿using Autofac;
using Rufus.AppFramework.AutofacExt;
using Rufus.AppFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework
{
    public class AppFrameworkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JsonParser>().AsSelf().SingleInstance().AutoConfigureLogger();
            base.Load(builder);
        }
    }
}
