﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public class TaskTracker : PropertyChangedBase, INotifyBusy
    {
        private class Tracker<T>
        {
            protected class TaskState
            {
            }

            private TaskCompletionSource<T> _tcs;
            protected TaskCompletionSource<T> Tcs
            {
                get { return _tcs; }
            }

            protected Tracker()
            {
                _tcs = new TaskCompletionSource<T>();
            }

            public static Task<T> Track(Task<T> task)
            {
                var tracker = new Tracker<T>();

                var state = new TaskState()
                {
                };
                task.ContinueWith(tracker.OnGenericTaskCompleted, state);

                return tracker.Tcs.Task;
            }

            private void OnGenericTaskCompleted(Task<T> task, object s)
            {
                var state = (TaskState)s;

                if (task.Status == TaskStatus.Faulted)
                {
                    Tcs.SetException(task.Exception);
                }
                else if (task.Status == TaskStatus.Canceled)
                {
                    Tcs.SetCanceled();
                }
                else if (task.Status == TaskStatus.RanToCompletion)
                {
                    Tcs.SetResult(task.Result);
                }
            }
        }

        private class Tracker : Tracker<object>
        {
            public static Task Track(Task task)
            {
                var tracker = new Tracker();
                var state = new TaskState()
                {
                };
                task.ContinueWith(tracker.OnNonGenericTaskCompleted, state);
                return tracker.Tcs.Task;
            }

            private void OnNonGenericTaskCompleted(Task task, object s)
            {
                var state = (TaskState)s;

                if (task.Status == TaskStatus.Faulted)
                {
                    Tcs.SetException(task.Exception);
                }
                else if (task.Status == TaskStatus.Canceled)
                {
                    Tcs.SetCanceled();
                }
                else if (task.Status == TaskStatus.RanToCompletion)
                {
                    Tcs.SetResult(null);
                }
            }
        }

        private readonly object _syncLock = new object();
        private int _waiting = 0;

        private bool _isBusy = false;
        public bool IsBusy
        {
            get { return _isBusy; }
            private set
            {
                if (_isBusy != value)
                {
                    _isBusy = value;
                    NotifyOfPropertyChange("IsBusy");
                }
            }
        }

        public TaskTracker()
        {
        }

        public Task TrackTask(Task t)
        {
            Task tracker = Tracker.Track(t);
            RegisterTrackedTask(tracker);
            return tracker;
        }

        public Task<T> TrackTask<T>(Task<T> t)
        {
            Task<T> tracker = Tracker<T>.Track(t);
            RegisterTrackedTask(tracker);
            return tracker;
        }

        private void RegisterTrackedTask(Task t)
        {
            lock (_syncLock)
            {
                _waiting++;
                t.ContinueWith(OnTaskCompleted);
            }
            IsBusy = _waiting != 0;
        }

        private void OnTaskCompleted(Task t)
        {
            lock (_syncLock)
            {
                _waiting--;
                // Should not happen.
                if (_waiting < 0)
                    _waiting = 0;
            }

            IsBusy = _waiting != 0;
        }
    }
}
