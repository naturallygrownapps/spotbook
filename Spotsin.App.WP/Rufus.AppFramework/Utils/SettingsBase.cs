﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public class SettingsBase : Caliburn.Micro.PropertyChangedBase
    {
        public T GetValue<T>(string key, T fallbackValue = default(T))
        {
            return SettingsStorage.GetValueOrFallback<T>(key, fallbackValue);
        }

        public void SetValue(string key, string value)
        {
            SettingsStorage.SetValue(key, value);
        }
    }
}
