﻿using System;

namespace Rufus.AppFramework.Utils
{
    public interface INotifyBusy : System.ComponentModel.INotifyPropertyChanged
    {
        bool IsBusy { get; }
    }
}
