﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public static class DoubleParser
    {
        public static double? Parse(string s)
        {
            double result;
            if (double.TryParse(s, System.Globalization.NumberStyles.Float, CultureInfo.InvariantCulture, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public static string ToString(double d)
        {
            return d.ToString(CultureInfo.InvariantCulture);
        }
    }
}
