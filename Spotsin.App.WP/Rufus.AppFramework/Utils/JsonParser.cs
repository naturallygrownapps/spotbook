﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public class JsonParser
    {
        private readonly JsonSerializerSettings _settings;

        public JsonParser()
        {
            _settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
        }

        public T Parse<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _settings);
        }

        public string ToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj, _settings);
        }
    }
}
