﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public static class ExceptionExtensions
    {
        public static bool ContainsInnerException(this Exception self, Type innerType)
        {
            return FindInnerException(self, innerType) != null;
        }

        public static Exception FindInnerException(this Exception self, Type innerType)
        {
            var current = self;

            if (current != null)
            {
                var aggregate = current as AggregateException;
                if (aggregate != null)
                {
                    foreach (var inner in aggregate.InnerExceptions)
                    {
                        if (inner != null && inner.GetType() == innerType)
                            return inner;

                        var ex = FindInnerException(inner, innerType);
                        if (ex != null)
                            return ex;
                    }
                }
                else
                {
                    var inner = current.InnerException;
                    if (inner != null && inner.GetType() == innerType)
                        return inner;
                    else
                        return FindInnerException(inner, innerType);
                }
            }

            return null;
        }

        /// <summary>
        /// Flattens an <see cref="AggregateException"/> and returns the first exception if
        /// there is only one exception in <see cref="AggregateException.InnerExceptions"/>.
        /// </summary>
        public static Exception TryFlattenAndUnwrap(this Exception self)
        {
            var aggregate = self as AggregateException;
            if (aggregate != null)
            {
                aggregate = aggregate.Flatten();
                if (aggregate.InnerExceptions.Count == 1)
                {
                    return aggregate.InnerExceptions.First();
                }
                else
                {
                    return aggregate;
                }
            }
            else
            {
                return self;
            }
        }

        /// <summary>
        /// Returns <see cref="string.Empty"/> if <paramref name="self"/> is null,
        /// a concatenation of the messages of all exceptions in an <see cref="AggregateException"/>
        /// or <see cref="Exception.Message"/> for any other exception.
        /// </summary>
        public static string GetFlattenedMessage(this Exception self)
        {
            if (self == null)
                return string.Empty;

            var aggregate = self as AggregateException;
            if (aggregate != null)
            {
                var msg = string.Join("\n", aggregate.InnerExceptions.Select(ex => ex.Message).Distinct());
                return msg;
            }
            else
            {
                return self.Message;
            }
        }
    }
}
