﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public static class SettingsStorage
    {
        public delegate bool TryGetValueHandler(string key, out object value);
        public delegate void SetValueHandler(string key, object value);


        private static TryGetValueHandler _tryGetValue = delegate(string k, out object v)
        {
            v = null;
            return false;
        };
        private static SetValueHandler _setValue = delegate(string k, object v)
        {
        };

        public static void SetStorageHandlers(TryGetValueHandler getHandler, SetValueHandler setHandler)
        {
            _tryGetValue = getHandler;
            _setValue = setHandler;
        }

        public static T GetValueOrFallback<T>(string key, T fallbackValue = default(T))
        {
            object entry;
            if (_tryGetValue(key, out entry) &&
                entry is T)
            {
                return (T)entry;
            }
            else
            {
                return fallbackValue;
            }
        }

        public static void SetValue(string key, string value)
        {
            _setValue(key, value);
        }
    }
}
