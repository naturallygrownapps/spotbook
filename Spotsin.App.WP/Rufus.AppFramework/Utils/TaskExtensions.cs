﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public static class TaskExtensions
    {
        public static Task Track(this Task task, TaskTracker taskTracker)
        {
            if (taskTracker != null && task != null)
            {
                return taskTracker.TrackTask(task);
            }
            else
            {
                return task;
            }
        }

        public static Task<T> Track<T>(this Task<T> task, TaskTracker taskTracker)
        {
            if (taskTracker != null && task != null)
            {
                return taskTracker.TrackTask(task);
            }
            else
            {
                return task;
            }
        }
    }
}
