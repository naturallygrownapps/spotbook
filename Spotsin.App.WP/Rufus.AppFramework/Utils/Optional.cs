﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    public struct Optional<T>
    {
        private readonly bool _hasValue;
        public bool HasValue
        {
            get { return _hasValue; }
        }

        private readonly T _value;
        public T Value
        {
            get { return _value; }
        }

        public Optional(T value)
        {
            _hasValue = true;
            _value = value;
        }

        public static implicit operator Optional<T>(T value)
        {
            return new Optional<T>(value);
        }
    }
}
