﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Utils
{
    /// <summary>
    /// Obtains a new <see cref="CancellationToken"/> on <see cref="Start"/> and cancels the previously returned token.
    /// </summary>
    public class SingleCancellationSource
    {
        private readonly object _SyncLock = new object();
        private CancellationTokenSource _currentCts;

        public CancellationToken Start()
        {
            lock (_SyncLock)
            {
                Cancel();

                _currentCts = new CancellationTokenSource();
                return _currentCts.Token;
            }
        }

        public void Cancel()
        {
            lock (_SyncLock)
            {
                if (_currentCts != null)
                {
                    try
                    {
                        _currentCts.Cancel();
                    }
                    catch { }
                    _currentCts = null;
                }
            }
        }
    }
}
