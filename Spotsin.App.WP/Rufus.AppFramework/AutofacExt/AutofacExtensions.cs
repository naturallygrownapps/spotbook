﻿using Autofac;
using Autofac.Builder;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.AutofacExt
{
    public static class AutofacExtensions
    {
        public static IRegistrationBuilder<TLimit, ConcreteReflectionActivatorData, SingleRegistrationStyle> 
            AutoConfigureLogger<TLimit>(this IRegistrationBuilder<TLimit, ConcreteReflectionActivatorData, SingleRegistrationStyle> self)
        {
            var resolvedParam = new ResolvedParameter((param, ctx) => param.ParameterType == typeof(MetroLog.ILogger),
                                                      (param, ctx) => ctx.Resolve<MetroLog.ILogManager>().GetLogger(typeof(TLimit)));
            return self.WithParameter(resolvedParam);
        }
    }
}
