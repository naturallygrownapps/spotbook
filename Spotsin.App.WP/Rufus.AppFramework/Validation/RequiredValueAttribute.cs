﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Rufus.AppFramework.Validation
{
    public class RequiredValueAttribute : ValidationAttribute
    {
        private static Dictionary<Type, object> _defaultValueCache = new Dictionary<Type, object>();

        private object _unsetValue;
        /// <summary>
        /// Gets or sets the value that defines the property as "unset" and thus invalid.
        /// </summary>
        public object UnsetValue
        {
            get { return _unsetValue; }
            set { _unsetValue = value; }
        }

        public RequiredValueAttribute()
        {
        }

        protected override bool Validate(object valueOwner, object value)
        {
            var unset = UnsetValue;
            if (unset == null)
            {
                // Automatically detect the default value.

                // String gets a special handling here. string.Empty should also be treated as "unset".
                var str = value as string;
                if (str != null)
                {
                    return !string.IsNullOrEmpty(str);
                }
                else
                {
                    if (value == null)
                    {
                        return false;
                    }
                    else
                    {
                        var defaultValue = GetDefaultValue(value.GetType());
                        return !object.Equals(value, defaultValue);
                    }
                }
            }
            else
            {
                return !object.Equals(value, unset);
            }
        }

        private static object GetDefaultValue(Type type)
        {
            object defaultValue;
            if (!_defaultValueCache.TryGetValue(type, out defaultValue))
            {
                defaultValue = MakeDefault(type);
                _defaultValueCache[type] = defaultValue;
            }
            return defaultValue;
        }

        private static object MakeDefault(Type type)
        {
            var makeDefault = typeof(RequiredValueAttribute).GetTypeInfo().GetDeclaredMethod("MakeDefaultGeneric");
            var closedMethod = makeDefault.MakeGenericMethod(type);
            return closedMethod.Invoke(null, new object[] { });
        }

        private static T MakeDefaultGeneric<T>()
        {
            return default(T);
        }
    }
}