﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Validation
{
    public enum RegexValidationPreset
    {
        None,
        /// <summary>
        /// Represents a regex that checks if a string forms a valid e-mail address.
        /// </summary>
        EMail,
        /// <summary>
        /// Represents a regex that checks if a string only consists of numeric characters.
        /// </summary>
        NumericChars,
        /// <summary>
        /// Represents a regex that checks a string for the existance of at least one lower-case letter,
        /// at least one upper-case letter and at least one number. 
        /// The string must also have a minimum length of 8 characters.
        /// </summary>
        CgmPassword
    }

    public class RegexValidationAttribute : TypedValidationAttribute
    {
        private static readonly Dictionary<RegexValidationPreset, string> _presets = new Dictionary<RegexValidationPreset, string>()
        {
            { RegexValidationPreset.EMail, @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}(#[0-9]*)?$" },
            { RegexValidationPreset.NumericChars, @"^\d*$" },
            { RegexValidationPreset.CgmPassword, @"^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d).{8,}" }
        };

        private string _pattern = ".*";
        /// <summary>
        /// Gets or sets the pattern to use for matching. Setting this property override <see cref="Preset"/>.
        /// </summary>
        public string Pattern
        {
            get { return _pattern; }
            set 
            { 
                _pattern = value;
                _preset = RegexValidationPreset.None;
                _regex = null; 
            }
        }

        private RegexValidationPreset _preset = RegexValidationPreset.None;
        /// <summary>
        /// Gets or sets a regex pattern preset. Setting this property overrides <see cref="Pattern"/>.
        /// </summary>
        public RegexValidationPreset Preset
        {
            get { return _preset; }
            set 
            { 
                string pattern;
                if (_presets.TryGetValue(value, out pattern))
                {
                    Pattern = pattern;
                }
                _preset = value;
            }
        }

        private Regex _regex;

        public RegexValidationAttribute()
        {
        }

        protected override bool ValidateCore(object valueOwner, object value)
        {
            var str = (string)value;

            var r = _regex;
            if (r == null)
            {
                r = new Regex(Pattern, RegexOptions.Singleline);
                _regex = r;
            }

            // We don't want to require a string but if we have a value, validate it.
            return string.IsNullOrEmpty(str) || r.IsMatch(str);
        }

        protected override Type GetValidatedType()
        {
            return typeof(string);
        }
    }
}
