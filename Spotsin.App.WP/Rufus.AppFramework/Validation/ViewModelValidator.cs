﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Rufus.AppFramework.Validation
{
    /// <summary>
    /// Provides automatic validation on view models by listening to <see cref="System.ComponentModel.INotifyPropertyChanged.PropertyChanged"/> events.
    /// Validation can also be triggered manually by calling <see cref="Validate"/>.
    /// Validations are defined by attributes deriving <see cref="ValidationAttribute"/> on properties of the view model.
    /// </summary>
    public class ViewModelValidator : System.ComponentModel.INotifyDataErrorInfo, System.ComponentModel.INotifyPropertyChanged
    {
        private class PropertyValidator
        {
            public readonly PropertyInfo Property;
            public readonly ValidationAttribute[] Attributes;
            public readonly object ViewModel;

            private IEnumerable<string> _errors;
            public IEnumerable<string> Errors
            {
                get { return _errors; }
            }

            private bool _isValid = true;
            public bool IsValid
            {
                get { return _isValid; }
                private set { _isValid = value; }
            }

            private readonly Func<object> _getter;

            public PropertyValidator(PropertyInfo property, ValidationAttribute[] attributes, object viewModel)
            {
                Property = property;
                Attributes = attributes;
                ViewModel = viewModel;

                _getter = MakeGetterFunc(Property, viewModel);
            }

            public void Validate(out bool success, out bool errorsHaveChanged)
            {
                success = true;
                List<string> newErrors = null;

                var value = _getter();
                foreach(var attr in Attributes)
                {
                    string curMsg;
                    bool result = attr.Validate(ViewModel, value, out curMsg);
                    if (!result)
                    {
                        if (newErrors == null)
                            newErrors = new List<string>();

                        // Record it.
                        if (!string.IsNullOrWhiteSpace(curMsg))
                            newErrors.Add(curMsg);
                    }

                    success &= result;
                }


                if (_errors == null)
                {
                    // No errors before.

                    if (newErrors != null)
                    {
                        // We have some errors now.
                        errorsHaveChanged = true;
                        _errors = newErrors;
                    }
                    else
                    {
                        // No new errors.
                        errorsHaveChanged = false;
                    }
                }
                else
                {
                    // We had errors before.

                    if (newErrors != null)
                    {
                        // Check if we got the same errors.
                        errorsHaveChanged = !_errors.SequenceEqual(newErrors);
                        if (errorsHaveChanged)
                        {
                            _errors = newErrors;
                        }
                    }
                    else
                    {
                        // No more errors now.
                        errorsHaveChanged = true;
                        _errors = null;
                    }
                }

                IsValid = success;
            }

            private static MethodInfo OpenMakeDelegateMethod;

            private static Func<object> MakeGetterFunc(PropertyInfo prop, object target)
            {
                if (OpenMakeDelegateMethod == null)
                {
                    OpenMakeDelegateMethod = typeof(PropertyValidator).GetTypeInfo().GetDeclaredMethod("MakeDelegate");
                }

                var closedMethod = OpenMakeDelegateMethod.MakeGenericMethod(prop.PropertyType);
                return (Func<object>)closedMethod.Invoke(null, new[] { prop.GetMethod, target });
            }

            private static Func<object> MakeDelegate<T>(MethodInfo getter, object target)
            {
                var f = (Func<T>)getter.CreateDelegate(typeof(Func<T>), target);
                return () => f();
            }
        }

        public class IndexerBindingHelper : System.ComponentModel.INotifyPropertyChanged
        {
            protected readonly ViewModelValidator _vmValidator;

            public IndexerBindingHelper(ViewModelValidator vmValidator)
            {
                _vmValidator = vmValidator;
            }

            internal void NotifyChanged()
            {
                // Notify that the indexer has changed.
                NotifyPropertyChanged("Item[]");
            }

            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

            protected void NotifyPropertyChanged(string propertyName)
            {
                var handler = this.PropertyChanged;
                if (handler != null)
                    handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        public class HasPropertyErrorHelper : IndexerBindingHelper
        {
            internal HasPropertyErrorHelper(ViewModelValidator vmValidator)
                : base(vmValidator)
            {
            }

            [System.Runtime.CompilerServices.IndexerName("Item")]
            public bool this[string propertyName]
            {
                get
                {
                    var errors = _vmValidator.GetErrors(propertyName) as IEnumerable<string>;
                    return errors != null;
                }
            }
        }

        public class HasPropertyErrorMessageHelper : IndexerBindingHelper
        {
            internal HasPropertyErrorMessageHelper(ViewModelValidator vmValidator)
                : base(vmValidator)
            {
            }

            [System.Runtime.CompilerServices.IndexerName("Item")]
            public string this[string propertyName]
            {
                get
                {
                    var errors = _vmValidator.GetErrors(propertyName) as IEnumerable<string>;
                    if (errors != null)
                    {
                        return string.Join(Environment.NewLine, errors);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        private readonly HasPropertyErrorHelper _isPropertyInvalid;
        /// <summary>
        /// Helper to bind single property validation states.
        /// </summary>
        /// <remarks>
        /// Usage: {Binding IsPropertyInvalid[propertyName]}
        /// </remarks>
        public HasPropertyErrorHelper IsPropertyInvalid
        {
            get { return _isPropertyInvalid; }
        }

        private readonly HasPropertyErrorMessageHelper _propertyErrors;
        /// <summary>
        /// Helper to bind to the error message for a single property.
        /// </summary>
        /// <remarks>
        /// Usage: {Binding PropertyErrors[propertyName]}
        /// </remarks>
        public HasPropertyErrorMessageHelper PropertyErrors
        {
            get { return _propertyErrors; }
        }

        private readonly object _viewModel;
        /// <summary>
        /// property name -> PropertyValidator for that property
        /// </summary>
        private readonly Dictionary<string, PropertyValidator> _validators = new Dictionary<string, PropertyValidator>();
        private bool _hasErrors = false;

        private bool _initialValidation = false;

        public ViewModelValidator(object viewModel, bool validateInitially = true)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");
            _viewModel = viewModel;
            _isPropertyInvalid = new HasPropertyErrorHelper(this);
            _propertyErrors = new HasPropertyErrorMessageHelper(this);
            Init();
            if (validateInitially)
            {
                // We only want to determine the overall validity on the first validation
                // but not mark fields as invalid yet.
                // With this flag set, the property validity states are not set.
                _initialValidation = true;
                Validate();
                _initialValidation = false;
            }
        }

        private void Init()
        {
            var type = _viewModel.GetType();
            // Find all properties with ValidationAttributes.
            foreach(var prop in type.GetRuntimeProperties())
            {
                var validationAttrs = prop.GetCustomAttributes().OfType<ValidationAttribute>();
                if (validationAttrs.Any())
                {
                    var validator = new PropertyValidator(prop, validationAttrs.ToArray(), _viewModel);
                    _validators[prop.Name] = validator;
                }
            }

            // Check if we can automatically validate properties when they change.
            var propChanged = _viewModel as System.ComponentModel.INotifyPropertyChanged;
            if (propChanged != null)
            {
                propChanged.PropertyChanged += OnViewModelPropertyChanged;
            }
        }

        private void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (_validators.ContainsKey(e.PropertyName))
            {
                Validate(e.PropertyName);
            }
        }

        /// <summary>
        /// Validates all properties of the view model.
        /// </summary>
        /// <returns>True if validation was successful, false else.</returns>
        public bool Validate()
        {
            bool success = true;
            foreach (var kv in _validators)
            {
                success &= Validate(kv.Key, kv.Value, false);
            }
            HasErrors = !success;
            return success;
        }

        /// <summary>
        /// Validates a single property of the view model.
        /// </summary>
        /// <param name="propertyName">The name of the property on the view model to validate.</param>
        /// <returns>True if validation was successful, false else.</returns>
        public bool Validate(string propertyName)
        {
            PropertyValidator validator;
            if (!_validators.TryGetValue(propertyName, out validator))
            {
                throw new KeyNotFoundException("Property not found for validation: '" + propertyName + "'");
            }
            return Validate(propertyName, validator, true);
        }

        private bool Validate(string propertyName, PropertyValidator validator, bool updateHasErrorsFlag)
        {
            bool result;
            bool errorsHaveChanged;
            validator.Validate(out result, out errorsHaveChanged);

            if (updateHasErrorsFlag)
            {
                UpdateHasErrorsFlag();
            }

            if (errorsHaveChanged)
            {
                NotifyErrorsChanged(propertyName);
            }

            return result;
        }

        private void UpdateHasErrorsFlag()
        {
            bool isValid = true;
            foreach(var kv in _validators)
            {
                isValid &= kv.Value.IsValid;
                if (!isValid)
                    break;
            }
            HasErrors = !isValid;
        }

        public event EventHandler<System.ComponentModel.DataErrorsChangedEventArgs> ErrorsChanged;

        private void NotifyErrorsChanged(string propertyName)
        {
            if (!_initialValidation)
            {
                var handler = this.ErrorsChanged;
                if (handler != null)
                {
                    handler(this, new System.ComponentModel.DataErrorsChangedEventArgs(propertyName));
                }

                // Notify of updates on the indexer helpers.
                _isPropertyInvalid.NotifyChanged();
                _propertyErrors.NotifyChanged();
            }
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            PropertyValidator validator;
            if (_validators.TryGetValue(propertyName, out validator))
            {
                return validator.Errors;
            }
            else
            {
                return null;
            }
        }

        public bool HasErrors
        {
            get { return _hasErrors; }
            set 
            { 
                _hasErrors = value; 
                NotifyPropertyChanged("HasErrors");
                NotifyPropertyChanged("IsValid");
            }
        }

        public bool IsValid
        {
            get { return !HasErrors; }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
