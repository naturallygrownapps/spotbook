﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rufus.AppFramework.Validation
{
    public class EqualsOtherValueAttribute : ValidationAttribute
    {
        private readonly string _otherPropertyName;
        public string OtherPropertyName
        {
            get { return _otherPropertyName; }
        }

        public EqualsOtherValueAttribute(string otherPropertyName)
        {
            if (string.IsNullOrWhiteSpace(otherPropertyName))
                throw new ArgumentNullException("otherPropertyName");
            _otherPropertyName = otherPropertyName;
        }

        protected override bool Validate(object valueOwner, object value)
        {
            if (valueOwner == null)
                return false;

            var valueOwnerType = valueOwner.GetType();
            var otherProp = valueOwnerType.GetRuntimeProperty(OtherPropertyName);
            if (otherProp == null)
                throw new ArgumentException("Could not find a property with name " + OtherPropertyName + " on object of type " + valueOwnerType.Name + ".");

            var otherValue = otherProp.GetValue(valueOwner);
            return object.Equals(value, otherValue);
        }
    }
}
