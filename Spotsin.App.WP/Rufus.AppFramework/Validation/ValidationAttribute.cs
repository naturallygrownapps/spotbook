﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Rufus.AppFramework.Validation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=true, Inherited=true)]
    public abstract class ValidationAttribute : Attribute
    {
        public string ErrorMessage { get; set; }

        public bool Validate(object valueOwner, object value, out string message)
        {
            bool isValid = Validate(valueOwner, value);
            message = isValid ? string.Empty : GetMessage();
            return isValid;
        }

        protected abstract bool Validate(object valueOwner, object value);

        public ValidationAttribute()
        {
            ErrorMessage = string.Empty;
        }

        protected string GetMessage()
        {
            return ErrorMessage;
        }
    }

    /// <summary>
    /// Checks if the provided validation value is of a specific type. Attribute types cannot be generic in C#
    /// so this is only a lousy workaround.
    /// </summary>
    public abstract class TypedValidationAttribute : ValidationAttribute
    {
        private bool HasValidType(object value)
        {
            if (value == null)
            {
                // We cannot get a type from null so simply assume true so that the 
                // implementation of ValidateCore can decide what to do with it.
                return true;
            }
            else
            {
                return GetValidatedType().GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo());
            }
        }

        protected override sealed bool Validate(object valueOwner, object value)
        {
            if (!HasValidType(value))
            {
                return false;
            }
            else
            {
                return ValidateCore(valueOwner, value);
            }
        }

        protected abstract bool ValidateCore(object valueOwner, object value);

        /// <summary>
        /// Gets the type of data that is expected to be validated.
        /// Only objects compatible to this type are passed to <see cref="ValidateCore"/>.
        /// </summary>
        protected abstract Type GetValidatedType();

    }

}
