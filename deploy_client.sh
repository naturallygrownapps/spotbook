#!/bin/sh

grunt --base ./SpotBookWeb/SpotBookWeb --gruntfile ./SpotBookWeb/SpotBookWeb/Gruntfile.js build
rsync -av ./SpotBookWeb/SpotBookWeb/dist/* /var/www/spotsin
chown -R www-data:www-data /var/www/spotsin

