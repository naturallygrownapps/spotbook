#!/bin/sh

xbuild /p:Configuration=Release ./SpotBookServer/SpotBookServer.sln
rsync -av --exclude='config.json' ./SpotBookServer/SpotBookServer/bin/Release/* /opt/mono/spotserver
