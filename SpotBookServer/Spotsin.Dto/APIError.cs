﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data
{
    public class APIError
    {
        public string ErrorMessage { get; set; }
        public string FullException { get; set; }
        public string ErrorCode { get; set; }
    }
}
