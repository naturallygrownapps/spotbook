﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spotsin.Data.Dto
{
    public class SpotDto
    {
        public string Id { get; set; }
        public string SpotBookId { get; set; }
        public string Summary { get; set; }
        public string Notes { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public LocationDto Location { get; set; }

        public SpotDto()
        {
        }
    }
}
