﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Dto
{
    public class LocationDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public LocationDto()
        {
        }
    }
}
