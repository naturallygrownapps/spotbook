﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Dto
{
    public partial class UserGrantedAccessRightDto
    {
        public UserEntityDto User { get; set; }
        public AccessRight Right { get; set; }

        public UserGrantedAccessRightDto()
        {
        }
    }
}
