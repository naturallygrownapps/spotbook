﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Dto
{
    public class UserEntityDto
    {
        public string Id { get; set; }
        public string Username { get; set; }

        public UserEntityDto()
        {
        }
    }
}
