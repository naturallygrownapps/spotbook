﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Dto
{
    public class SpotBookDto
    {
        public string Id { get; set; }
        public UserEntityDto Creator { get; set; }
        public List<UserGrantedAccessRightDto> Rights { get; set; }
        public string Name { get; set; }

        public SpotBookDto()
        {
        }
    }
}
