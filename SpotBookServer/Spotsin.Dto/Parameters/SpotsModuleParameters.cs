﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Parameters
{
    public class SpotsModuleParameters
    {
        public class GetSpotInput
        {
            public string SpotId { get; set; }
        }

        public class CreateSpotInput : Spotsin.Data.Dto.SpotDto
        {
        }

        public class DeleteSpotInput
        {
            public string SpotId { get; set; }
        }

        public class DeleteSpotOutput
        {
        }
    }
}
