﻿using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Parameters
{
    public class SpotBooksModuleParameters
    {
        public static class ErrorCodes
        {
            public const string UserNotFound = "UserNotFound";
            public const string SpotBookNotFound = "SpotBookNotFound";
            public const string SpotBookAccessDenied = "SpotBookAccessDenied";
        }

        public class CreateSpotBookInput
        {
            public string Name { get; set; }
        }

        public class GetSpotBooksInput
        {
        }

        public class GetSpotBooksOutput : List<SpotBookDto>
        {
        }

        public class GetSingleSpotBookInput
        {
            public string SpotBookId { get; set; }
        }

        public class SearchSpotBooksInput
        {
            public string SearchString { get; set; }
        }

        public class SearchSpotBooksOutput : List<SpotBookDto>
        {
        }

        public class AddAccessRightInput
        {
            public string SpotBookId { get; set; }
            public string Username { get; set; } 
            public string AccessRight { get; set; }
        }

        public class AddAccessRightOutput
        {
        }

        public class DeleteAccessRightInput
        {
            public string SpotBookId { get; set; }
            public string Username { get; set; }
        }

        public class DeleteAccessRightOutput
        {
        }
    }
}
