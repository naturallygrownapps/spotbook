﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data.Parameters
{
    public class GeocodingModuleParameters
    {
        public class GeocodeInput
        {
            public string Street { get; set; }
            public string PostalCode { get; set; }
            public string City { get; set; }
        }
    }
}
