﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Spotsin.Data.Parameters
{
    public class AuthModuleParameters
    {
        public class LoginInput
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class LoginOutput
        {
            public string Token { get; set; }
        }

        public class RegisterInput
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class RegisterOutput
        {
            public string UserId { get; set; }
        }
    }
}