﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spotsin.Data
{
    [Flags]
    public enum AccessRight
    {
        Read = 1, 
        Write = 2,
        ReadWrite = Read | Write
    }
}
