﻿using Newtonsoft.Json;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace SpotBookServer
{
    public class SBJsonContractResolver : Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver
    {
        private readonly Dictionary<Type, HashSet<string>> _ignoredPropsMap = new Dictionary<Type, HashSet<string>>();

        protected override IList<Newtonsoft.Json.Serialization.JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var list = base.CreateProperties(type, memberSerialization);
            var ignoredPropNames = GetIgnoredProperties(type);
            if (ignoredPropNames.Count > 0)
            {
                List<Newtonsoft.Json.Serialization.JsonProperty> toRemove = null;
                foreach (var prop in list)
                {
                    if (ignoredPropNames.Contains(prop.UnderlyingName))
                    {
                        if (toRemove == null)
                        {
                            toRemove = new List<Newtonsoft.Json.Serialization.JsonProperty>();
                        }
                        toRemove.Add(prop);
                    }
                }

                if (toRemove != null)
                {
                    foreach (var r in toRemove)
                    {
                        list.Remove(r);
                    }
                }
            }
            return list;
        }

        private HashSet<string> GetIgnoredProperties(Type type)
        {
            HashSet<string> result;
            if (!_ignoredPropsMap.TryGetValue(type, out result))
            {
                var attrs = type.GetCustomAttributes<IgnoreBaseClassPropertyAttribute>(false);
                result = new HashSet<string>(attrs.Select(a => a.PropertyName));
                _ignoredPropsMap[type] = result;
            }
            return result;
        }
    }

    public class SBJsonSerializer : JsonSerializer
    {
        public SBJsonSerializer()
        {
            this.ContractResolver = new SBJsonContractResolver();
            this.Converters.Add(new ObjectIdConverter());
            this.Converters.Add(new GeoJsonConverter());
        }
    }
}