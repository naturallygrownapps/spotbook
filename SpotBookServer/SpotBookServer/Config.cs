﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer
{
    public class Config
    {
        private string _dbConnectionString;
        public string DbConnectionString
        {
            get { return _dbConnectionString; }
            set { _dbConnectionString = value; }
        }

        private string _dbName;
        public string DbName
        {
            get { return _dbName; }
            set { _dbName = value; }
        }

        private string _logPath;
        public string LogPath
        {
            get { return _logPath; }
            set { _logPath = value; }
        }

        public Config()
        {
            try
            {
                if (File.Exists("config.json"))
                {
                    var cfgStr = File.ReadAllText("config.json");
                    JsonConvert.PopulateObject(cfgStr, this);
                }
                else
                {
                    // TODO: Log me.
                }
            }
            catch(Exception)
            {
                // TODO: Log me.
            }
        }
    }
}
