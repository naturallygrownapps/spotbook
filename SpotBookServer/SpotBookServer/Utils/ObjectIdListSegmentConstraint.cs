﻿using MongoDB.Bson;
using Nancy.Routing.Constraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public class ObjectIdListSegmentConstraint : RouteSegmentConstraintBase<List<ObjectId>>
    {
        public override string Name
        {
            get { return "idlist"; }
        }

        protected override bool TryMatch(string constraint, string segment, out List<ObjectId> matchedValue)
        {
            if (constraint != Name)
            {
                matchedValue = null;
                return false;
            }

            if (string.IsNullOrWhiteSpace(segment))
            {
                matchedValue = new List<ObjectId>();
                return true;
            }
            else
            {
                var split = segment.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                List<ObjectId> result = new List<ObjectId>(split.Length);
                foreach (var s in split)
                {
                    ObjectId id;
                    if (ObjectId.TryParse(s, out id))
                    {
                        result.Add(id);
                    }
                }
                matchedValue = result;
                return true;
            }

        }
    }
}
