﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public static class TileMath
    {
        public const int DefaultZoom = 14;

        //public static BoundingBox Tile2BoundingBox(int x, int y, int zoom = DefaultZoom)
        //{
        //    return GetBoundingBox(x, y, 1, 1, zoom);
        //}

        //public static BoundingBox GetBoundingBox(int x, int y, int width, int height, int zoom = DefaultZoom)
        //{
        //    BoundingBox bb = new BoundingBox();
        //    bb.Top = Tile2lat(y, zoom);
        //    bb.Bottom = Tile2lat(y + height, zoom);
        //    bb.Left = Tile2lon(x, zoom);
        //    bb.Right = Tile2lon(x + width, zoom);
        //    return bb;
        //}

        private static double Tile2lon(int x, int z)
        {
            return x / Math.Pow(2.0, z) * 360.0 - 180;
        }

        private static double Tile2lat(int y, int z)
        {
            double n = Math.PI - (2.0 * Math.PI * y) / Math.Pow(2.0, z);
            return (Math.Atan(Math.Sinh(n))) * 180.0 / Math.PI;
        }
    }
}
