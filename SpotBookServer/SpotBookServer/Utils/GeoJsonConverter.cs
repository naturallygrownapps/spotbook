﻿using MongoDB.Driver.GeoJsonObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpotBookServer.Modules.Spots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public class GeoJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(GeoJsonPoint<GeoJson2DGeographicCoordinates>) == objectType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            var coords = token["coordinates"];
            var lat = coords.Value<double>("latitude");
            var lon = coords.Value<double>("longitude");
            return GeoJsonHelper.Create(lat, lon);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var loc = (GeoJsonPoint<GeoJson2DGeographicCoordinates>)value;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("Point");
            writer.WritePropertyName("coordinates");

            writer.WriteStartObject();
            writer.WritePropertyName("latitude");
            writer.WriteValue(loc.Coordinates.Latitude);
            writer.WritePropertyName("longitude");
            writer.WriteValue(loc.Coordinates.Longitude);
            writer.WriteEndObject();

            writer.WriteEndObject();
        }

        public override bool CanWrite
        {
            get { return true; }
        }
    }
}
