﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    /// <summary>
    /// Exempts a property from being serialized. Useful to prevent certain properties from being sent to clients.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class IgnoreBaseClassPropertyAttribute : Attribute
    {
        public string PropertyName { get; set; }

        public IgnoreBaseClassPropertyAttribute()
        {
        }

        public IgnoreBaseClassPropertyAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }
    }
}
