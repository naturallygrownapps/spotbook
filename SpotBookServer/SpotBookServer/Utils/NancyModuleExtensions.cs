﻿using Nancy;
using Nancy.ModelBinding;
using SpotBookServer.Modules.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SpotBookServer.Utils
{
    public static class NancyModuleExtensions
    {
        public static dynamic Call<TInput>(this INancyModule module, Func<TInput, dynamic> handler)
        {
            var input = module.Bind<TInput>();
            return handler(input);
        }

        public static Task<dynamic> CallAsync<TInput>(this INancyModule module, Func<TInput, CancellationToken, Task<dynamic>> handler, CancellationToken ct)
        {
            var input = module.Bind<TInput>();
            return handler(input, ct);
        }
    }
}