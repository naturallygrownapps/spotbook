﻿using Nancy;
using SpotBookServer.Modules.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public static class ServiceExtensions
    {
        public static UserEntity GetCurrentUser(this IUserDataService self, INancyModule module)
        {
            UserEntity result = null;
            var userIdentity = module.Context.CurrentUser;
            if (userIdentity != null)
            {
                result = self.Find(userIdentity.UserName);
            }

            if (result == null)
                throw new InvalidOperationException("Could not find the current user. Are you logged in?");

            return result;
        }
    }
}
