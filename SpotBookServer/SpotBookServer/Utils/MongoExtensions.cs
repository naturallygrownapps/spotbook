﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpotBookServer.Utils
{
    public static class MongoExtensions
    {
        public static WriteConcernResult CheckSuccess(this WriteConcernResult result)
        {
            if (!string.IsNullOrWhiteSpace(result.ErrorMessage))
            {
                throw new MongoException(result.ErrorMessage);
            }
            return result;
        }
    }
}