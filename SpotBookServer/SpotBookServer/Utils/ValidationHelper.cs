﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public static class ValidationHelper
    {
        public static string LimitString(string str, int length)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length <= length)
                return str;

            return str.Substring(0, length);
        }
    }
}
