﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpotBookServer.Utils
{
    public class GuidTools
    {
        public static string GetGuid()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}