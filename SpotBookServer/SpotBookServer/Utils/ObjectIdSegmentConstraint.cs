﻿using MongoDB.Bson;
using Nancy.Routing.Constraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public class ObjectIdSegmentConstraint : RouteSegmentConstraintBase<ObjectId>
    {
        public override string Name
        {
            get { return "objectid"; }
        }

        protected override bool TryMatch(string constraint, string segment, out ObjectId matchedValue)
        {
            if (constraint != Name || string.IsNullOrWhiteSpace(segment))
            {
                matchedValue = ObjectId.Empty;
                return false;
            }

            return ObjectId.TryParse(segment, out matchedValue);
        }
    }
}
