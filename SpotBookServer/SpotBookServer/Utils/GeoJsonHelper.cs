﻿using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SpotBookServer.Utils
{
    public static class GeoJsonHelper
    {
        public static readonly GeoJsonPoint<GeoJson2DGeographicCoordinates> Empty = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(0.0, 0.0));

        public static GeoJsonPoint<GeoJson2DGeographicCoordinates> Create(double lat, double lon)
        {
            return new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(lon, lat));
        }

        public static bool TryParse(string latlon, out GeoJsonPoint<GeoJson2DGeographicCoordinates> location)
        {
            location = null;
            if (string.IsNullOrWhiteSpace(latlon))
                return false;

            if (!latlon.Contains(","))
                return false;

            var split = latlon.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length != 2)
                return false;

            double lat;
            if (!double.TryParse(split[0], NumberStyles.Float, CultureInfo.InvariantCulture, out lat))
                return false;

            double lon;
            if (!double.TryParse(split[1], NumberStyles.Float, CultureInfo.InvariantCulture, out lon))
                return false;

            location = GeoJsonHelper.Create(lat, lon);
            return true;
        }
    }
}
