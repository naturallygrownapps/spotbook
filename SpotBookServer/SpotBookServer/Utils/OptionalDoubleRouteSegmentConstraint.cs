﻿using Nancy.Routing.Constraints;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public class OptionalDoubleRouteSegmentConstraint : RouteSegmentConstraintBase<double?>
    {
        public override string Name
        {
            get { return "optdouble"; }
        }

        protected override bool TryMatch(string constraint, string segment, out double? matchedValue)
        {
            if (constraint != Name)
            {
                matchedValue = null;
                return false;
            }

            double parsed;
            if (double.TryParse(segment, 
                                System.Globalization.NumberStyles.Float,  
                                CultureInfo.InvariantCulture,
                                out parsed))
            {
                matchedValue = parsed;
                return true;
            }
            else
            {
                matchedValue = null;
                return false;
            }
        }
    }
}
