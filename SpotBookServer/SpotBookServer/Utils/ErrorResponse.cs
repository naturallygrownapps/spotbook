﻿using Nancy;
using Nancy.Responses;
using Newtonsoft.Json;
using Spotsin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpotBookServer.Utils
{
    public class ErrorResponse : JsonResponse
    {
        readonly APIError error;

        private ErrorResponse(APIError error)
            : base(error, new DefaultJsonSerializer())
        {
            this.error = error;
            this.StatusCode = HttpStatusCode.InternalServerError;
        }

        public string ErrorMessage { get { return error.ErrorMessage; } }
        public string FullException { get { return error.FullException; } }
        public string ErrorCode { get { return error.ErrorCode; } }
        //public string[] Errors { get { return error.Errors; } }

        public static ErrorResponse FromMessage(string message,
                                                HttpStatusCode statusCode = HttpStatusCode.InternalServerError,
                                                string errorCode = null)
        {
            return new ErrorResponse(new APIError { ErrorMessage = message, ErrorCode = errorCode })
                {
                    StatusCode = statusCode
                };
        }

        public static ErrorResponse FromException(Exception ex,
                                                  HttpStatusCode statusCode = HttpStatusCode.InternalServerError,
                                                  string errorCode = null)
        {
            var exception = ex.GetBaseException();

            var summary = exception.Message;
            if (exception is System.Net.WebException || exception is System.Net.Sockets.SocketException)
            {
                summary = "Network connection error: " + summary;
            }

            var error = new APIError 
            { 
                ErrorMessage = summary, 
                FullException = exception.ToString(),
                ErrorCode = errorCode
            };

            var response = new ErrorResponse(error);
            response.StatusCode = statusCode;
            return response;
        }
    }
}
