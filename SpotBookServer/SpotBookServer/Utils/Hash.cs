﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public static class Hash
    {
        public static string Sha256(string message)
        {
            SHA256Managed crypt = new SHA256Managed();
            StringBuilder hashStr = new StringBuilder();
            var bytes = Encoding.UTF8.GetBytes(message);
            byte[] hashedBytes = crypt.ComputeHash(bytes, 0, bytes.Length);
            for (int i = 0; i < hashedBytes.Length; i++)
            {
                var b = hashedBytes[i];
                hashStr.Append(b.ToString("X2"));
            }
            return hashStr.ToString();
        }
    }
}
