﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public struct BoundingBox
    {
        public double MinX;
        public double MinY;
        public double MaxX;
        public double MaxY;
    }
}
