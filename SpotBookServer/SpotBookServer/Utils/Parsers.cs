﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Utils
{
    public static class Parsers
    {
        public static IEnumerable<ObjectId> ParseObjectIds(string segment)
        {
            if (string.IsNullOrWhiteSpace(segment))
            {
                return Enumerable.Empty<ObjectId>();
            }
            else
            {
                var split = segment.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                List<ObjectId> result = new List<ObjectId>(split.Length);
                foreach (var s in split)
                {
                    ObjectId id;
                    if (ObjectId.TryParse(s, out id))
                    {
                        result.Add(id);
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Parses an enum from a string which is either a enum value name (preferred method)
        /// or the underlying enum value (e.g. int, byte, ...).
        /// </summary>
        /// <typeparam name="T">The enum type. Must be an enum.</typeparam>
        /// <param name="value">The string to parse.</param>
        /// <returns>The parsed enum value or null if no value could be parsed from the <paramref name="value"/>.</returns>
        public static T? ParseEnum<T>(string value)
            where T : struct
        {
            if (!typeof(T).IsEnum)
                throw new InvalidOperationException("Must be called with an enum.");

            T result;
            if (!Enum.TryParse<T>(value, true, out result))
            {
                var underlyingType = Enum.GetUnderlyingType(typeof(T));
                try
                {
                    var underlyingValue = Convert.ChangeType(value, underlyingType);
                    return (T)underlyingValue;
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return result;
            }
        }

        public static double? ParseNullableDouble(string segment)
        {
            double parsed;
            if (TryParseDouble(segment, out parsed))
            {
                return parsed;
            }
            else
            {
                return null;
            }
        }

        public static bool TryParseDouble(string segment, out double result)
        {
            return double.TryParse(segment, NumberStyles.Float, CultureInfo.InvariantCulture, out result);
        }

        public static bool TryParseTilePos(string segment, out TilePos tilePos)
        {
            if (!string.IsNullOrEmpty(segment))
            {
                var split = segment.Split(',');
                if (split.Length == 2)
                {
                    int x;
                    int y;
                    if (int.TryParse(split[0], out x) &&
                        int.TryParse(split[1], out y))
                    {
                        tilePos = new TilePos(x, y);
                        return true;
                    }
                }
            }

            tilePos = default(TilePos);
            return false;
        }

        public static bool TryParseBoundingBox(string segment, out BoundingBox bb)
        {
            if (!string.IsNullOrEmpty(segment))
            {
                var split = segment.Split(',');
                if (split.Length == 4)
                {
                    double minX;
                    double minY;
                    double maxX;
                    double maxY;
                    if (TryParseDouble(split[0], out minX) &&
                        TryParseDouble(split[1], out minY) &&
                        TryParseDouble(split[2], out maxX) &&
                        TryParseDouble(split[3], out maxY))
                    {
                        bb = new BoundingBox()
                        {
                            MinX = minX,
                            MinY = minY,
                            MaxX = maxX,
                            MaxY = maxY
                        };
                        return true;
                    }
                }
            }

            bb = default(BoundingBox);
            return false;
        }
    }
}
