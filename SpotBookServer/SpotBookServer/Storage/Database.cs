﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpotBookServer
{
    public class Database
    {
        private readonly Config _cfg;
        private readonly MongoClient _client;
        private readonly MongoDatabase _db;

        public Database(Config cfg)
        {
            _cfg = cfg;
            var connStr = _cfg.DbConnectionString;
            if (string.IsNullOrWhiteSpace(connStr))
                throw new ArgumentException("The configured DbConnectionString must not be empty.");
            if (string.IsNullOrWhiteSpace(_cfg.DbName))
                throw new ArgumentException("The configured DbName must not be empty.");
            _client = new MongoClient(connStr);
            var server = _client.GetServer();
            _db = server.GetDatabase(_cfg.DbName);
        }

        public MongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }
}