﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace SpotBookServer.Modules.Auth
{
    public interface IUserDataService
    {
        ObjectId Add(UserEntity userEntity);
        UserEntity Find(string username);
        UserEntity FindById(ObjectId id);
        IEnumerable<UserEntity> FindLike(string username);
    }
}
