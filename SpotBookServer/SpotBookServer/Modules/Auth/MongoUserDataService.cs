﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver.Linq;

namespace SpotBookServer.Modules.Auth
{
    public class MongoUserDataService : IUserDataService
    {
        private readonly MongoCollection<UserEntity> _userCollection;

        public MongoUserDataService(Database db)
        {
            _userCollection = db.GetCollection<UserEntity>("users");
        }

        public UserEntity Find(string username)
        {
            return _userCollection.FindOne(Query<UserEntity>.EQ(u => u.Username, username));
        }

        public UserEntity FindById(ObjectId id)
        {
            return _userCollection.FindOneById(id);
        }

        public ObjectId Add(UserEntity userEntity)
        {
            var result = _userCollection.Insert(userEntity);
            return result.Upserted != null ? result.Upserted.AsObjectId : userEntity.Id;
        }

        public IEnumerable<UserEntity> FindLike(string username)
        {
            return _userCollection.Find(Query<UserEntity>.Matches(u => u.Username, new BsonRegularExpression("^" + username)));
        }
    }
}