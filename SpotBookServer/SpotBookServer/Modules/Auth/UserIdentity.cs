﻿using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpotBookServer.Modules.Auth
{
    public class UserIdentity : IUserIdentity
    {
        public IEnumerable<string> Claims
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public UserIdentity()
        {
            Claims = Enumerable.Empty<string>();
        }

        public UserIdentity(UserEntity userEntity)
            : this()
        {
            UserName = userEntity.Username;
        }
    }
}