﻿using MongoDB.Bson;
using SpotBookServer.Utils;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Auth
{
    public partial class UserEntity
    {
        public static UserEntityDto MakeDto(UserEntity userEntity)
        {
            return new UserEntityDto()
            {
                Id = userEntity.Id.ToString(),
                Username = userEntity.Username
            };
        }
    }
}
