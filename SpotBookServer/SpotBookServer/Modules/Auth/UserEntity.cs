﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpotBookServer.Modules.Auth
{
    public partial class UserEntity
    {
        public ObjectId Id { get; set; }
        public string Username { get; set; }
        public string PwHash { get; set; }

        public UserEntity()
        {
            Id = ObjectId.Empty;
        }
    }
}