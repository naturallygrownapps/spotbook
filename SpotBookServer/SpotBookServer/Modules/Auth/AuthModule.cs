﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Nancy;
using Nancy.Authentication.Token;
using Nancy.ModelBinding;
using SpotBookServer.Modules.Spots;
using SpotBookServer.Utils;
using Spotsin.Data;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SpotBookServer.Modules.Auth
{
    public partial class AuthModule : NancyModule
    {
        private readonly IUserDataService _userSvc;
        private readonly ISpotBookDataService _spotBookDataSvc;
        private readonly ITokenizer _tokenizer;

        public AuthModule(IUserDataService userSvc, ISpotBookDataService spotBookDataSvc, ITokenizer tokenizer)
            : base("/auth")
        {
            _userSvc = userSvc;
            _spotBookDataSvc = spotBookDataSvc;
            _tokenizer = tokenizer;

            Post["/login"] = _ => this.Call<AuthModuleParameters.LoginInput>(Login);
            Post["/register"] = _ => this.Call<AuthModuleParameters.RegisterInput>(Register);
        }

        private dynamic Login(AuthModuleParameters.LoginInput input)
        {
            if (string.IsNullOrWhiteSpace(input.Username) ||
                string.IsNullOrWhiteSpace(input.Password))
            {
                return ErrorResponse.FromMessage("Missing username and/or password.", HttpStatusCode.BadRequest);
            }

            var username = ValidationHelper.LimitString(input.Username, 50).ToLower();
            var password = ValidationHelper.LimitString(input.Password, 500);

            var existingEntity = _userSvc.Find(username);
            var hashedPw = Hash.Sha256(password);
            if (existingEntity != null && existingEntity.PwHash == hashedPw)
            {
                var user = new UserIdentity(existingEntity);
                var token = _tokenizer.Tokenize(user, Context);
                return new AuthModuleParameters.LoginOutput()
                {
                    Token = token
                };
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private dynamic Register(AuthModuleParameters.RegisterInput input)
        {
            if (string.IsNullOrWhiteSpace(input.Username) ||
                string.IsNullOrWhiteSpace(input.Password))
            {
                return ErrorResponse.FromMessage("Missing username and/or password.", HttpStatusCode.BadRequest);
            }

            var username = ValidationHelper.LimitString(input.Username, 50).ToLower();
            var password = ValidationHelper.LimitString(input.Password, 500);

            var existing = _userSvc.Find(username);
            if (existing != null)
            {
                return ErrorResponse.FromMessage("Sorry, that username is already taken. Please choose a different name.").
                                     WithStatusCode(HttpStatusCode.Conflict);
            }
            else
            {
                var hashedPw = Hash.Sha256(password);
                var newUser = new UserEntity()
                {
                    Username = username,
                    PwHash = hashedPw
                };
                var createdUserId = _userSvc.Add(newUser);

                // Create a spotbook for the new user so that he can start right away.
                var sbName = username + (username.EndsWith("s") ? "'" : "s") + " Spots";
                var sb = new SpotBook()
                {
                    Name = ValidationHelper.LimitString(sbName, 100),
                    CreatorId = createdUserId,
                    Rights = new List<UserGrantedAccessRight>()
                    {
                        new UserGrantedAccessRight()
                        {
                            UserId = createdUserId,
                            Right = AccessRight.ReadWrite
                        }
                    }
                };

                var createdSb = _spotBookDataSvc.Add(sb);

                return Negotiate.WithModel(new AuthModuleParameters.RegisterOutput()
                                          {
                                              UserId = createdUserId.ToString()
                                          }).
                                 WithStatusCode(HttpStatusCode.Created);
            }
        }

    }
}