﻿using Nancy;
using Nancy.Security;
using Newtonsoft.Json.Linq;
using SpotBookServer.Utils;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Geo
{
    public partial class GeocodingModule : NancyModule
    {
        private const string BaseUrl = "http://dev.virtualearth.net/REST/v1/Locations/DE";
        private const string ApiKey = "Ag1oYpUldQJm55SIdzrMRib_NSWHrPxNdQ8KitdHJXRQo01v8Bf1UhrL_RuFAFtu";

        private static readonly HttpClient _http = new HttpClient();

        public GeocodingModule()
        {
            this.RequiresAuthentication();

            Get["/geocode/{street}/{city}/{postalCode?}", true] = (_, ct) =>
                this.CallAsync<GeocodingModuleParameters.GeocodeInput>(Geocode, ct);
        }

        private async Task<dynamic> Geocode(GeocodingModuleParameters.GeocodeInput parameters, CancellationToken ct)
        {
            string street = parameters.Street;
            string postalCode = parameters.PostalCode;
            if (string.IsNullOrWhiteSpace(postalCode) || postalCode == "undefined")
            {
                postalCode = "-";
            }
            string city = parameters.City;

            var url = BaseUrl + "/" + postalCode + "/" + city + "/" + street + "?key=" + ApiKey;
            var responseStr = await _http.GetStringAsync(url);

            dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(responseStr);
            try
            {
                var coordinates = json.resourceSets[0].resources[0].point.coordinates;
                double lat = coordinates[0];
                double lon = coordinates[1];

                return Response.AsJson(new
                {
                    lat,
                    lon
                });
            }
            catch(ArgumentOutOfRangeException)
            {
                return HttpStatusCode.NotFound;
            }
            catch(NullReferenceException)
            {
                return HttpStatusCode.NotFound;
            }
        }
    }
}
