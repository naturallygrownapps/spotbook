﻿using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public partial class Spot
    {
		public static SpotDto MakeDto(Spot spot)
        {
            return new SpotDto()
            {
                Address = spot.Address,
                Id = spot.Id.ToString(),
                Location = new LocationDto()
                {
                    Latitude = spot.Location.Coordinates.Latitude,
                    Longitude = spot.Location.Coordinates.Longitude
                },
				Notes = spot.Notes,
				SpotBookId = spot.SpotBookId.ToString(),
				Summary = spot.Summary,
				Type = spot.Type
            };
        }
    }
}
