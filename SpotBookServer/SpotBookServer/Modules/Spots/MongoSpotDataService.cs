﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GeoJsonObjectModel;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public class MongoSpotDataService: ISpotDataService
    {
        private readonly MongoCollection<Spot> _spotCollection;

        public MongoSpotDataService(Database db)
        {
            _spotCollection = db.GetCollection<Spot>("spots");
            
            var indexKey = IndexKeys.GeoSpatialSpherical("location");
            if (!_spotCollection.IndexExists(indexKey))
            {
                _spotCollection.CreateIndex(indexKey);
            }
        }

        //public IEnumerable<Spot> Search(IEnumerable<ObjectId> spotBookIds, GeoJsonPoint<GeoJson2DGeographicCoordinates> location, double radius)
        //{
        //    if (spotBookIds == null)
        //        return Enumerable.Empty<Spot>();

        //    var filterBySpotBookIds = Query<Spot>.In(s => s.SpotBookId, spotBookIds);
        //    var filterByDistance = Query.Near("location", location, radius, true);
        //    return _spotCollection.Find(Query.And(filterBySpotBookIds, filterByDistance));
        //}

        public IEnumerable<Spot> Search(IEnumerable<ObjectId> spotBookIds, 
                                        BoundingBox? bb)
        {
            if (spotBookIds == null)
                return Enumerable.Empty<Spot>();

            var filterBySpotBookIds = Query<Spot>.In(s => s.SpotBookId, spotBookIds);
            IMongoQuery query;
            if (bb.HasValue)
            {
                var bbValue = bb.Value;
                var filterByBB = Query.WithinRectangle("location", bbValue.MinX, bbValue.MaxY, bbValue.MaxX, bbValue.MinY);
                query = Query.And(filterByBB, filterBySpotBookIds);
            }
            else
            {
                query = filterBySpotBookIds;
            }

            return _spotCollection.Find(query);
        }

        public Spot Find(ObjectId spotId)
        {
            return _spotCollection.FindOneById(spotId);
        }

        public Spot Add(Spot spot)
        {
            var result = _spotCollection.Insert(spot);
            return spot;
        }

        public bool Delete(ObjectId spotId)
        {
            var query = Query<Spot>.EQ(s => s.Id, spotId);
            var result = _spotCollection.Remove(query);
            return result.Ok;
        }

        public void Delete(IEnumerable<ObjectId> spotIds)
        {
            throw new NotImplementedException();
        }
    }
}
