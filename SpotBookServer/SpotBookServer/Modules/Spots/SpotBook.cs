﻿using MongoDB.Bson;
using Spotsin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public partial class SpotBook
    {
        public ObjectId Id { get; set; }
        public ObjectId CreatorId { get; set; }
        public List<UserGrantedAccessRight> Rights { get; set; }
        public string Name { get; set; }

        public SpotBook()
        {
            Id = ObjectId.Empty;
        }

        public UserGrantedAccessRight GetRightForUser(ObjectId userId)
        {
            var r = Rights;
            if (r == null)
                return null;
            else
                return r.FirstOrDefault(u => u.UserId.Equals(userId));
        }

        public bool IsWriteAllowedFor(ObjectId userId)
        {
            var r = GetRightForUser(userId);
            return r != null && r.Right.HasFlag(AccessRight.Write);
        }

        public bool IsReadAllowedFor(ObjectId userId)
        {
            var r = GetRightForUser(userId);
            return r != null && r.Right.HasFlag(AccessRight.Read);
        }

        public bool AddRight(ObjectId userId, AccessRight right)
        {
            var existingRight = GetRightForUser(userId);
            if (existingRight == null)
            {
                var r = Rights;
                if (r == null)
                {
                    r = new List<UserGrantedAccessRight>();
                    Rights = r;
                }

                r.Add(new UserGrantedAccessRight()
                {
                    Right = right,
                    UserId = userId
                });
                return true;
            }
            else if (!existingRight.Right.HasFlag(right))
            {
                existingRight.Right = right;
                return true;
            }

            return false;
        }

        public bool RemoveRight(ObjectId userId)
        {
            var existingRight = GetRightForUser(userId);
            if (existingRight != null)
            {
                var r = Rights;
                if (r != null)
                {
                    r.Remove(existingRight);
                    return true;
                }
            }

            return false;
        }
    }
}
