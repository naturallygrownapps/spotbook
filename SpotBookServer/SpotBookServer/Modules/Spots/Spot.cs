﻿using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpotBookServer.Modules.Spots
{
    public partial class Spot
    {
        public ObjectId Id { get; set; }
        public ObjectId SpotBookId { get; set; }
        public string Summary { get; set; }
        public string Notes { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }

        public Spot()
        {
            Id = ObjectId.Empty;
            Location = GeoJsonHelper.Create(0.0, 0.0);
        }
    }
}
