﻿using MongoDB.Bson;
using Spotsin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public partial class UserGrantedAccessRight
    {
        public ObjectId UserId { get; set; }
        public AccessRight Right { get; set; }

        public UserGrantedAccessRight()
        {
        }
    }
}
