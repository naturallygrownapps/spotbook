﻿using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public interface ISpotDataService
    {
        IEnumerable<Spot> Search(IEnumerable<ObjectId> spotBookIds, 
                                 BoundingBox? bb);
        Spot Find(ObjectId spotId);
        Spot Add(Spot spot);
        bool Delete(ObjectId spotId);
        void Delete(IEnumerable<ObjectId> spotIds);
    }
}
