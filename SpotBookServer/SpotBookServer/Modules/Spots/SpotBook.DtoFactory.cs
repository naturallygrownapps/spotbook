﻿using MongoDB.Bson;
using SpotBookServer.Modules.Auth;
using SpotBookServer.Utils;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public partial class SpotBook
    {
        public static SpotBookDto MakeDto(SpotBook spotbook,
                                          IUserDataService userDataSvc)
        {
            var dto = new SpotBookDto()
            {
                Id = spotbook.Id.ToString(),
                Name = spotbook.Name
            };
            var creator = userDataSvc.FindById(spotbook.CreatorId);
            dto.Creator = UserEntity.MakeDto(creator);
            dto.Rights = spotbook.Rights.Select(r => UserGrantedAccessRight.MakeDto(r, userDataSvc)).ToList();
            return dto;
        }

        public static void MakeDto(IEnumerable<SpotBook> spotbooks,
                                   IUserDataService userDataSvc,
                                   IList<SpotBookDto> toPopulate)
        {
            foreach(var sb in spotbooks)
            {
                toPopulate.Add(SpotBook.MakeDto(sb, userDataSvc));
            }
        }

		public static IEnumerable<SpotBookDto> MakeDto(IEnumerable<SpotBook> spotbooks,
                                                       IUserDataService userDataSvc)
        {
            var toPopulate = new List<SpotBookDto>();
            SpotBook.MakeDto(spotbooks, userDataSvc, toPopulate);
            return toPopulate;
        }
    }
}
