﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public interface ISpotBookDataService
    {
        SpotBook Find(ObjectId id);
        IEnumerable<SpotBook> FindAll(IEnumerable<ObjectId> ids);
        IEnumerable<SpotBook> FindForUser(ObjectId userId);
        SpotBook Add(SpotBook spotBook);
        void Delete(SpotBook spotBook);
        void Update(SpotBook spotBook);
        IEnumerable<SpotBook> Search(string searchString);
        //UserGrantedAccessRight GetRightsForUser(ObjectId spotBookId, ObjectId userId);
    }
}
