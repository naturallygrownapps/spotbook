﻿using MongoDB.Bson;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using SpotBookServer.Modules.Auth;
using SpotBookServer.Utils;
using Spotsin.Data;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpotBookServer.Modules.Spots
{
    public partial class SpotBooksModule : NancyModule
    {
        private readonly ISpotBookDataService _spotBookData;
        private readonly ISpotDataService _spotData;
        private readonly IUserDataService _userDataSvc;

        public SpotBooksModule(ISpotBookDataService spotBookData,
                               ISpotDataService spotData,
                               IUserDataService userDataSvc)
        {
            _spotBookData = spotBookData;
            _spotData = spotData;
            _userDataSvc = userDataSvc;

            this.RequiresAuthentication();

            Get["/spotbooks"] = _ =>
            {
                //?search={search}
                var search = Request.Query.search;
                if (search is string)
                {
                    var input = new SpotBooksModuleParameters.SearchSpotBooksInput()
                    {
                        SearchString = (string)search
                    };
                    return SearchSpotBooks(input);
                }
                else
                {
                    return this.Call<SpotBooksModuleParameters.GetSpotBooksInput>(GetSpotBooks);
                }
            };
            Get["/spotbooks/{spotbookid:objectid}"] = _ => this.Call<SpotBooksModuleParameters.GetSingleSpotBookInput>(GetSingleSpotBook);
            Post["/spotbooks"] = _ => this.Call<SpotBooksModuleParameters.CreateSpotBookInput>(CreateSpotBook);
            Delete["/spotbooks/{spotbookid:objectid}"] = _ =>
            {
                var spotBookId = ObjectId.Parse(_.spotbookid);
                return DeleteSpotBook(spotBookId);
            };
            Post["/spotbooks/{spotbookid:objectid}/rights/{username}"] = _ => this.Call<SpotBooksModuleParameters.AddAccessRightInput>(AddAccessRight);
            Delete["/spotbooks/{spotbookid:objectid}/rights/{username}"] = _ => this.Call<SpotBooksModuleParameters.DeleteAccessRightInput>(DeleteAccessRight);
        }

        private dynamic SearchSpotBooks(SpotBooksModuleParameters.SearchSpotBooksInput input)
        {
            return ErrorResponse.FromMessage("Searching public SpotBooks is not implemented yet.", HttpStatusCode.NotImplemented);
            //var output = new SearchSpotBooksOutput();
            //return output;
        }

        private dynamic GetSingleSpotBook(SpotBooksModuleParameters.GetSingleSpotBookInput input)
        {
            var sb = _spotBookData.Find(ObjectId.Parse(input.SpotBookId));
            if (sb == null)
                return ErrorResponse.FromMessage("Could not find specified SpotBook.", HttpStatusCode.NotFound, SpotBooksModuleParameters.ErrorCodes.SpotBookNotFound);

            // User must be logged in.
            var user = _userDataSvc.GetCurrentUser(this);
            if (!sb.IsReadAllowedFor(user.Id))
                return ErrorResponse.FromMessage("You don't have the access rights to read this book.", HttpStatusCode.Forbidden);

            return SpotBook.MakeDto(sb, _userDataSvc);
        }

        private dynamic GetSpotBooks(SpotBooksModuleParameters.GetSpotBooksInput input)
        {
            var books = _spotBookData.FindForUser(_userDataSvc.GetCurrentUser(this).Id);
            var output = new SpotBooksModuleParameters.GetSpotBooksOutput();
            SpotBook.MakeDto(books, _userDataSvc, output);
            return output;
        }

        private dynamic CreateSpotBook(SpotBooksModuleParameters.CreateSpotBookInput input)
        {
            if (string.IsNullOrWhiteSpace(input.Name))
                return ErrorResponse.FromMessage("Name must not be empty.", HttpStatusCode.BadRequest);

            var curUserId = _userDataSvc.GetCurrentUser(this).Id;
            var sb = new SpotBook()
            {
                Name = ValidationHelper.LimitString(input.Name, 100),
                CreatorId = curUserId,
                Rights = new List<UserGrantedAccessRight>()
                {
                    new UserGrantedAccessRight()
                    {
                        UserId = curUserId,
                        Right = AccessRight.ReadWrite
                    }
                }
            };

            var created = _spotBookData.Add(sb);
            var dto = SpotBook.MakeDto(created, _userDataSvc);
            return Response.AsJson(dto, HttpStatusCode.Created);
        }

        private dynamic DeleteSpotBook(ObjectId spotBookId)
        {
            var sb = _spotBookData.Find(spotBookId);
            if (sb == null)
                return ErrorResponse.FromMessage("Could not find specified SpotBook.", HttpStatusCode.NotFound, SpotBooksModuleParameters.ErrorCodes.SpotBookNotFound);

            // Only creators can delete spotbooks.
            var currentUser = _userDataSvc.GetCurrentUser(this);
            if (sb.CreatorId != currentUser.Id)
                return ErrorResponse.FromMessage("Only the creator can delete a SpotBook.", HttpStatusCode.Unauthorized, SpotBooksModuleParameters.ErrorCodes.SpotBookAccessDenied);

            _spotBookData.Delete(sb);

            return HttpStatusCode.OK;
        }

        private dynamic AddAccessRight(SpotBooksModuleParameters.AddAccessRightInput input)
        {
            var sb = _spotBookData.Find(ObjectId.Parse(input.SpotBookId));
            if (sb == null)
                return ErrorResponse.FromMessage("Could not find specified SpotBook.", HttpStatusCode.NotFound, SpotBooksModuleParameters.ErrorCodes.SpotBookNotFound);

            var newRight = Parsers.ParseEnum<AccessRight>(input.AccessRight);
            if (!newRight.HasValue)
                return ErrorResponse.FromMessage("Invalid access right specified.", HttpStatusCode.BadRequest);

            if (string.IsNullOrWhiteSpace(input.Username))
                return ErrorResponse.FromMessage("Missing username.", HttpStatusCode.BadRequest);

            var userEntity = _userDataSvc.Find(input.Username.ToLower());
            if (userEntity == null)
                return ErrorResponse.FromMessage("Could not find user.", HttpStatusCode.NotFound, SpotBooksModuleParameters.ErrorCodes.UserNotFound);

            // Only creators can change access rights.
            var currentUser = _userDataSvc.GetCurrentUser(this);
            if (sb.CreatorId != currentUser.Id)
                return ErrorResponse.FromMessage("Only the creator can change a SpotBook.", HttpStatusCode.Unauthorized, SpotBooksModuleParameters.ErrorCodes.SpotBookAccessDenied);

            if (sb.AddRight(userEntity.Id, newRight.Value))
            {
                _spotBookData.Update(sb);
            }

            return HttpStatusCode.OK;
        }

        private dynamic DeleteAccessRight(SpotBooksModuleParameters.DeleteAccessRightInput input)
        {
            var sb = _spotBookData.Find(ObjectId.Parse(input.SpotBookId));
            if (sb == null)
                return ErrorResponse.FromMessage("Could not find specified SpotBook.", HttpStatusCode.NotFound);

            var userEntity = _userDataSvc.Find(input.Username.ToLower());
            if (userEntity == null)
                return ErrorResponse.FromMessage("Could not find user.", HttpStatusCode.NotFound, SpotBooksModuleParameters.ErrorCodes.UserNotFound);

            // Only creators can change access rights.
            var currentUser = _userDataSvc.GetCurrentUser(this);
            if (sb.CreatorId != currentUser.Id)
                return ErrorResponse.FromMessage("Only the creator can change a SpotBook.", HttpStatusCode.Unauthorized, SpotBooksModuleParameters.ErrorCodes.SpotBookAccessDenied);

            if (sb.RemoveRight(userEntity.Id))
            {
                _spotBookData.Update(sb);

                return HttpStatusCode.OK;
                //return Response.AsJson(new DeleteAccessRightOutput()
                //{
                //}, HttpStatusCode.OK);
            }
            else
            {
                return ErrorResponse.FromMessage("The specified user does not have any access rights.", HttpStatusCode.NotFound);
            }
        }
    }
}
