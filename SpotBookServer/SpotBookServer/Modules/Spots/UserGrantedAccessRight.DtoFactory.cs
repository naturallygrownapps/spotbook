﻿using MongoDB.Bson;
using SpotBookServer.Modules.Auth;
using Spotsin.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public partial class UserGrantedAccessRight
    {
        public static UserGrantedAccessRightDto MakeDto(UserGrantedAccessRight userGrantedAccessRight,
                                                        IUserDataService userDataSvc)
        {
            var dto = new UserGrantedAccessRightDto()
            {
                Right = userGrantedAccessRight.Right
            };
            var userEntity = userDataSvc.FindById(userGrantedAccessRight.UserId);
            dto.User = UserEntity.MakeDto(userEntity);
            return dto;
        }
    }
}
