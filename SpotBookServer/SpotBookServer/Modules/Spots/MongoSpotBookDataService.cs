﻿using SpotBookServer.Utils;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotBookServer.Modules.Spots
{
    public class MongoSpotBookDataService : ISpotBookDataService
    {
        private readonly MongoCollection<SpotBook> _spotBookCollection;
        private readonly MongoCollection<Spot> _spotCollection;

        public MongoSpotBookDataService(Database db)
        {
            _spotBookCollection = db.GetCollection<SpotBook>("spotbooks");
            _spotCollection = db.GetCollection<Spot>("spots");
        }

        public SpotBook Find(ObjectId id)
        {
            return _spotBookCollection.FindOneById(id);
        }

        public IEnumerable<SpotBook> FindAll(IEnumerable<ObjectId> ids)
        {
            var query = Query<SpotBook>.In(sb => sb.Id, ids);
            return _spotBookCollection.Find(query);
        }

        public IEnumerable<SpotBook> FindForUser(ObjectId userId)
        {
            var query = Query<SpotBook>.Where(sb => sb.Rights != null && sb.Rights.Any(r => r.UserId.Equals(userId)));
            return _spotBookCollection.Find(query);
        }

        public SpotBook Add(SpotBook spotBook)
        {
            var result = _spotBookCollection.Insert(spotBook);
            return spotBook;
        }


        public void Delete(SpotBook spotBook)
        {
            var idToRemove = spotBook.Id;
            var query = Query<SpotBook>.EQ(sb => sb.Id, idToRemove);
            _spotBookCollection.Remove(query).CheckSuccess();

            var spotsToRemoveQuery = Query<Spot>.EQ(s => s.SpotBookId, idToRemove);
            _spotCollection.Remove(spotsToRemoveQuery).CheckSuccess();
        }

        //public UserGrantedAccessRight GetRightsForUser(ObjectId spotBookId, ObjectId userId)
        //{
        //    var sb = _spotBookCollection.FindOneById(spotBookId);
        //    if (sb != null)
        //    {
        //        return sb.Rights.FirstOrDefault(r => r.UserId == userId);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public void Update(SpotBook spotBook)
        {
            _spotBookCollection.Save(spotBook).CheckSuccess();
        }

        public IEnumerable<SpotBook> Search(string searchString)
        {
            throw new NotImplementedException("We currently only return all SpotBooks that a user has access to. No custom lists.");
            //var query = Query<SpotBook>.Matches(sb => sb.Name, BsonRegularExpression.Create(searchString));
        }
    }
}
