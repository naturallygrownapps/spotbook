﻿using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using SpotBookServer.Modules.Auth;
using SpotBookServer.Utils;
using Spotsin.Data;
using Spotsin.Data.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpotBookServer.Modules.Spots
{
    public partial class SpotsModule : NancyModule
    {
        private readonly ISpotDataService _spotData;
        private readonly ISpotBookDataService _spotBookData;
        private readonly IUserDataService _userDataSvc;

        public SpotsModule(ISpotDataService spotStorage, 
                          ISpotBookDataService spotBookData,
                          IUserDataService userDataSvc)
        {
            _spotData = spotStorage;
            _spotBookData = spotBookData;
            _userDataSvc = userDataSvc;

            this.RequiresAuthentication();

            // Search for spots by query string.
            Get["/spots"] = _ =>
            {
                //?spotbooks={spotbookids:IEnumerable<ObjectId>}&bb={bb:BoundingBox}
                var spotBookIds = Parsers.ParseObjectIds(Request.Query.spotbooks);

                BoundingBox bb;
                if (!Parsers.TryParseBoundingBox(Request.Query.bb, out bb))
                {
                    return ErrorResponse.FromMessage("Invalid query.", HttpStatusCode.BadRequest, ErrorCodes.InvalidQuery);
                }

                return FindSpots(spotBookIds, bb);
            };
            // Get a single spot.
            Get["/spots/{spotid:objectid}"] = _ =>
            {
                var input = new SpotsModuleParameters.GetSpotInput();
                input.SpotId = ObjectId.Parse(_.spotid);
                return GetSpot(input);
            };
            // Create a new spot.
            Post["/spots"] = _ => this.Call<SpotsModuleParameters.CreateSpotInput>(CreateSpot);
            // Remove a spot.
            Delete["/spots/{spotid:objectid}"] = _ => this.Call<SpotsModuleParameters.DeleteSpotInput>(DeleteSpot);
        }

        private dynamic FindSpots(IEnumerable<ObjectId> spotBookIds, BoundingBox bb)
        {   
            var userId = _userDataSvc.GetCurrentUser(this).Id;
            var spotBooks = _spotBookData.FindAll(spotBookIds);
            var validatedIds = new List<ObjectId>();

            foreach(var sb in spotBooks)
            {
                if (sb.IsReadAllowedFor(userId))
                {
                    validatedIds.Add(sb.Id);
                }
            }

            var spots = _spotData.Search(validatedIds, bb);
            return Response.AsJson(spots.Select(s => Spot.MakeDto(s)), HttpStatusCode.OK);
        }

        private dynamic GetSpot(SpotsModuleParameters.GetSpotInput input)
        {
            var s = _spotData.Find(ObjectId.Parse(input.SpotId));
            if (s != null)
            {
                return Response.AsJson(Spot.MakeDto(s), HttpStatusCode.OK);
            }
            else
            {
                return HttpStatusCode.NotFound;
            }
        }

        private dynamic CreateSpot(SpotsModuleParameters.CreateSpotInput toCreate)
        {
            // Perform checks.
            // Spot data needed.
            if (toCreate == null ||
                toCreate.Location == null ||
                string.IsNullOrWhiteSpace(toCreate.Summary))
            {
                return ErrorResponse.FromMessage("Invalid POST data received.", HttpStatusCode.BadRequest);
            }

            // User must be logged in.
            var user = _userDataSvc.GetCurrentUser(this);

            // New spot must have valid SpotBook reference.
            var sb = _spotBookData.Find(ObjectId.Parse(toCreate.SpotBookId));
            if (sb == null)
                return ErrorResponse.FromMessage("Missing or incorrect SpotBookId.", HttpStatusCode.BadRequest);

            // User must have write access to the book.
            if (!sb.IsWriteAllowedFor(user.Id))
                return ErrorResponse.FromMessage("You don't have the access rights to add spots to this book.", HttpStatusCode.Forbidden);

            var newSpot = new Spot()
            {
                Address = ValidationHelper.LimitString(toCreate.Address, 500),
                Location = GeoJsonHelper.Create(toCreate.Location.Latitude, toCreate.Location.Longitude),
                Notes = ValidationHelper.LimitString(toCreate.Notes, 1000),
                Summary = ValidationHelper.LimitString(toCreate.Summary, 500),
                Type = ValidationHelper.LimitString(toCreate.Type, 500),
                SpotBookId = sb.Id
            };
            var created = _spotData.Add(newSpot);

            return Response.AsJson(Spot.MakeDto(created), HttpStatusCode.Created);
        }

        private dynamic DeleteSpot(SpotsModuleParameters.DeleteSpotInput input)
        {
            if (_spotData.Delete(ObjectId.Parse(input.SpotId)))
            {
                return HttpStatusCode.OK;
            }
            else
            {
                return HttpStatusCode.NotFound;
            }
        }
    }
}
