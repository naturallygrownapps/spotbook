﻿using log4net;
using Nancy;
using Nancy.Authentication.Token;
using Nancy.Authentication.Token.Storage;
using Nancy.Bootstrapper;
using Nancy.Diagnostics;
using Nancy.TinyIoc;
using SpotBookServer.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SpotBookServer
{
    public class SBBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register(typeof(Newtonsoft.Json.JsonSerializer), typeof(SBJsonSerializer));

            // Configure logging.
            bool loggingConfigured = false;
            var loggerFile = new System.IO.FileInfo("log.xml");
            if (loggerFile.Exists)
            {
                try
                {
                    var cfg = container.Resolve<Config>();
                    var logFile = cfg.LogPath;

                    var logXmlStr = File.ReadAllText(loggerFile.FullName);
                    logXmlStr = logXmlStr.Replace("{LOGFILE}", logFile);

                    var xml = new System.Xml.XmlDocument();
                    xml.LoadXml(logXmlStr);
                    log4net.Config.XmlConfigurator.Configure(xml.DocumentElement);
                    loggingConfigured = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error configuring logging. " + ex.ToString());
                }
            }

            if (!loggingConfigured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            var logger = LogManager.GetLogger("SpotsIn");
            pipelines.OnError.AddItemToEndOfPipeline((context, exception) =>
            {
                logger.Error("Unhandled exception", exception);
                return null;
            });
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                            .WithHeader("Access-Control-Allow-Methods", "POST,GET,DELETE");

                if (ctx.Request.Method == "OPTIONS")
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Headers",
                                            "Accept, Origin, Content-type, Authorization");
                }

            });

            pipelines.OnError.AddItemToEndOfPipeline((z, a) =>
            {
                return ErrorResponse.FromException(a);
            });

            TokenAuthentication.Enable(pipelines, new TokenAuthenticationConfiguration(container.Resolve<ITokenizer>()));

            base.RequestStartup(container, pipelines, context);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);

            var instanceRegs = new InstanceRegistration[]
            {
                new InstanceRegistration(typeof(ITokenizer), new Tokenizer(cfg => cfg.WithKeyCache(new InMemoryTokenKeyStore()).
                                                                                  AdditionalItems(ctx => ctx.Request.UserHostAddress)))
            };
            RegisterInstances(container, instanceRegs);

            // Tell the mongodb library to store properties with a starting lower case letter
            var camelCaseConventionPack = new MongoDB.Bson.Serialization.Conventions.ConventionPack { new MongoDB.Bson.Serialization.Conventions.CamelCaseElementNameConvention() };
            MongoDB.Bson.Serialization.Conventions.ConventionRegistry.Register("CamelCase", camelCaseConventionPack, type => true);
        }

        protected override DiagnosticsConfiguration DiagnosticsConfiguration
        {
            get { return new DiagnosticsConfiguration { Password = @"Tawr2$1LK" }; }
        }
    }
}
